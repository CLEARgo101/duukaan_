<?php
/**
 * Yireo EmailOverride for Magento
 *
 * @package     Yireo_EmailOverride
 * @author      Yireo (https://www.yireo.com/)
 * @copyright   Copyright 2015 Yireo (https://www.yireo.com/)
 * @license     Open Source License (OSL v3)
 */

// Allow for an override of Aschroder_SMTPPro_Model_Email_Template
if (Mage::helper('core')->isModuleEnabled('Aschroder_SMTPPro') && class_exists('Aschroder_SMTPPro_Model_Email_Template')) {
    class Yireo_EmailOverride_Model_Email_Template_Wrapper extends Aschroder_SMTPPro_Model_Email_Template {}
}elseif (Mage::helper('core')->isModuleEnabled('Aschroder_Email') && class_exists('Aschroder_Email_Model_Email_Template')) {
    class Yireo_EmailOverride_Model_Email_Template_Wrapper extends Aschroder_Email_Model_Email_Template {}
}elseif (Mage::helper('core')->isModuleEnabled('Ebizmarts_Mandrill') && class_exists('Ebizmarts_Mandrill_Model_Email_Template')) {
    class Yireo_EmailOverride_Model_Email_Template_Wrapper extends Ebizmarts_Mandrill_Model_Email_Template {}
}elseif (Mage::helper('core')->isModuleEnabled('FreeLunchLabs_MailGun') && class_exists('FreeLunchLabs_MailGun_Model_Email_Template')) {
    class Yireo_EmailOverride_Model_Email_Template_Wrapper extends FreeLunchLabs_MailGun_Model_Email_Template {}
} else {
    class Yireo_EmailOverride_Model_Email_Template_Wrapper extends Mage_Core_Model_Email_Template {}
}

/**
 * EmailOverride Core model
 */
class Yireo_EmailOverride_Model_Email_Template extends Yireo_EmailOverride_Model_Email_Template_Wrapper
{
    public function setDesignConfig(array $config)
    {
        if(!isset($config['store'])) {
            /*if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) // store level
            {
                $store_id = Mage::getModel('core/store')->load($code)->getId();
            }
            elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) // website level
            {
                $website_id = Mage::getModel('core/website')->load($code)->getId();
                $store_id = Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
            }
            else // default level
            {
                $store_id = 0;
            } */

            $store_ids = Mage::getModel('core/store')->getCollection()->load()->getAllIds();
            if(!empty($store_ids)) {
                $config['store'] = $store_ids[0];
            }
        }
        if(isset($config['store'])) {
            $store = Mage::registry('emailoverride.store');
            if(empty($store)) {
                Mage::register('emailoverride.store', $config['store'], true);
            }
        }

        return parent::setDesignConfig($config);
    }
}
