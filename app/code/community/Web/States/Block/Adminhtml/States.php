<?php
class Web_States_Block_Adminhtml_States extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_states';
        $this->_blockGroup = 'web_states';
        $this->_headerText = Mage::helper('web_states')->__('Country/States Manager');
        parent::__construct();

        $this->_addButton('import', array(
            'label'         => Mage::helper('web_states')->__('Import'),
            'onclick'       => 'document.getElementById(\'uploadTarget\').click();',
            'class'         => 'add',
            'after_html'    =>
                '<form method="POST" action="'.$this->getUrl('*/*/import').'" id="uploadForm" enctype="multipart/form-data">'.
                '<input name="form_key" type="hidden" value="'.Mage::getSingleton("core/session")->getFormKey().'" />'.
                '<input type="file" name="uploadTarget" style="display:none;" id="uploadTarget" accept=".csv"/>'.
                '</form>'.
                '<script type="text/javascript">'.
                'document.getElementById(\'uploadTarget\').addEventListener(\'change\', function(){'.
                'if($("uploadTarget").value != "" && typeof $("uploadTarget").value != "undefined"){'.
                'document.getElementById(\'uploadForm\').submit();'.
                '}'.
                '}, false);'.
                '</script>',
        ));
    }
}