<?php

// app/code/local/Envato/Custompaymentmethod/controllers/PaymentController.php
class KA_Jambopaypaymentmethod_PaymentController extends Mage_Core_Controller_Front_Action
{
	public function gatewayAction()
	{
		if ($this->getRequest()->get("orderId")) {
			$arr_querystring = array(
					'flag' => 1,
					'orderId' => $this->getRequest()->get("orderId")
			);

			Mage_Core_Controller_Varien_Action::_redirect('jambopaypaymentmethod/payment/response', array('_secure' => false, '_query' => $arr_querystring));
		}
	}

	public function redirectAction()
	{
		$order = new Mage_Sales_Model_Order();
		$payment_info['order_id'] = Mage::getSingleton('checkout/session')->getLastRealOrderId();
//        var_dump($payment_info['order_id']); die();
		if ($payment_info['order_id'] == null) {
			$lastQuoteId = Mage::getSingleton('checkout/session')->getLastQuoteId();
			$orderCollection = Mage::getModel('sales/order')->getCollection();
			$orderCollection->addFieldToFilter('quote_id', array('eq' => $lastQuoteId));
			$orders = $orderCollection->getItems();
			$increment_ids = array();
			$amount = 0;
			foreach ($orders as $item) {

//            $order->loadByIncrementId( $item->getId() );
				//var_dump($item->getIncrementId());
				$increment_id = $item->getIncrementId();
				if (isset($increment_id)) {
					$increment_ids[] = $increment_id;
				}
				$amount += $item->base_grand_total;
				$payment_info['customer_email'] = $item->customer_email;
			}
			if (!empty($increment_ids)) {
				$payment_info['order_id'] = implode('_', $increment_ids);
			}
			$payment_info['amount'] = $amount;
		} else {
			$order->loadByIncrementId($payment_info['order_id']);
			$payment_info['amount'] = round($order->base_grand_total, 2);
			$payment_info['customer_email'] = $order->customer_email;
		}


		$payment_info['gateway_url'] = Mage::helper('jambopaypaymentmethod')->getPaymentGatewayUrl();
		$payment_info['business_email'] = Mage::helper('jambopaypaymentmethod')->getBusinessEmail();

		$store = Mage::app()->getStore();
		$payment_info['store_name'] = 'Duukaan';

		Mage::register('payment_info', $payment_info);


		$this->loadLayout();
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'jambopaypaymentmethod', array('template' => 'jambopaypaymentmethod/redirect.phtml'));
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
	}

	public function responseAction()
	{
		$debug = false;
		//************************ CHECK IF VALUES HAVE BEEN SET *****************
		if ($debug == false) {
			if (!isset($_REQUEST['JP_PASSWORD'])) {
				$this->_redirectUrl(Mage::getBaseUrl() . 'jambopaypaymentmethod/payment/fail');
				return;
			}
		}

		$JP_TRANID = $_REQUEST['JP_TRANID'];
		$JP_MERCHANT_ORDERID = $_REQUEST['JP_MERCHANT_ORDERID'];
		$JP_ITEM_NAME = $_REQUEST['JP_ITEM_NAME'];
		$JP_AMOUNT = $_REQUEST['JP_AMOUNT'];
		$JP_CURRENCY = $_REQUEST['JP_CURRENCY'];
		$JP_TIMESTAMP = $_REQUEST['JP_TIMESTAMP'];
		$JP_PASSWORD = $_REQUEST['JP_PASSWORD'];
		$JP_CHANNEL = $_REQUEST['JP_CHANNEL'];

		//$sharedkey, IS ONLY SHARED BETWEEN THE MERCHANT AND JAMBOPAY. THE KEY SHOULD BE SECRET ********************
		//Make sure you get the key from JamboPay Support team
		$sharedkey = Mage::helper('jambopaypaymentmethod')->getSecrecKey();

		$str = $JP_MERCHANT_ORDERID . $JP_AMOUNT . $JP_CURRENCY . $sharedkey . $JP_TIMESTAMP;

		if ($debug == false) {
			//**************** VERIFY *************************
			if (md5(utf8_encode($str)) != $JP_PASSWORD) {
				$this->_redirectUrl(Mage::getBaseUrl() . 'jambopaypaymentmethod/payment/fail');
				return;
			}
		}
		//$JP_TRANID = '12';
		//VALID
		//if valid, mark order as paid

		$list_order['order_id'] = array();
		$list_order['order_id'] = explode('_', $JP_MERCHANT_ORDERID);

		foreach ($list_order['order_id'] as $m_order) {
			$_order = Mage::getModel('sales/order')->loadByIncrementId($m_order);
			$this->createTransactionForOrder($_order, $JP_TRANID);
			$this->createInvoiceForOrder($_order);
            if($_order->getPayment()->getMethodInstance()->getCode()=="jambopaypaymentmethod") {
                $_order->sendNewOrderEmail();
                Mage::helper('vendorssales')->sendNewOrderEmail($_order);
            }
		}

		//$_SESSION['PAYMENT_RESPONSE_CODE'] = $_REQUEST['vpc_ResponseCode'];
		$this->_redirectUrl(Mage::getBaseUrl() . 'checkout/onepage/success');
		return;
	}

	private function createTransactionForOrder($_order, $tran_no)
	{
		try {
			$payment = $_order->getPayment();
			if ($payment != false) {
				$payment->setTransactionId($tran_no);
				$transaction = $payment->addTransaction('payment', null, false, '');
				$transaction->setParentTxnId($tran_no);
				$transaction->setIsClosed(0);
				//$transaction->setAdditionalInformation("arrI    nfo", serialize($arrInformation));
				$transaction->save();
				$_order->save();
			}
		} catch (Exception $e) {

		}
	}

	private function createInvoiceForOrder($_order)
	{
		try {
			if (!$_order->canInvoice()) {
				Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
			}
			$invoice = Mage::getModel('sales/service_order', $_order)->prepareInvoice();
			if (!$invoice->getTotalQty()) {
				Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
			}
			$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
			$invoice->register();
			$invoice->sendEmail();

			$transactionSave = Mage::getModel('core/resource_transaction')
					->addObject($invoice)
					->addObject($invoice->getOrder());

			$transactionSave->save();

            $_order->setState(Mage_Sales_Model_Order::STATE_PROCESSING)
                ->setStatus(Mage_Sales_Model_Order::STATE_PROCESSING)
                ->save();
		} catch (Exception $e) {

		}
	}

	public function failAction()
	{
		$this->loadLayout();
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'jambopaypaymentmethod', array('template' => 'jambopaypaymentmethod/fail.phtml'));
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
		//var_dump('<pre>', $this->getRequest(), '</pre>'); die();
	}

	public function cancelAction()
	{
		$this->loadLayout();
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'jambopaypaymentmethod', array('template' => 'jambopaypaymentmethod/cancel.phtml'));
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
		//var_dump('<pre>', $this->getRequest(), '</pre>'); die();
	}
}