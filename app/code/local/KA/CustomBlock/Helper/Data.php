<?php
class KA_CustomBlock_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function renderProductItem($product)
    {
        $output = '';

        $output .= '<div class="product-thumbnail">';

        $output .= '<a href="'.$product->getProductUrl().'">';
        $product_img = '';
        try {
            $product_img = Mage::helper('catalog/image')->init($product, 'thumbnail');
        } catch (Exception $e) {
            //Mage::log('Image '.$e->getMessage(), null, 'image_exception.log');
        }
        $output .= '<img src="'.$product_img.'" width="150" height="150" /></a>';

        $now = date("Y-m-d H:m:S");
        // Get the Special Price
        $specialprice = Mage::getModel('catalog/product')->load($product->getId())->getSpecialPrice();
        $price = Mage::getModel('catalog/product')->load($product->getId())->getPrice();
        // Get the Special Price FROM date
        $specialPriceFromDate = Mage::getModel('catalog/product')->load($product->getId())->getSpecialFromDate();
        // Get the Special Price TO date
        $specialPriceToDate = Mage::getModel('catalog/product')->load($product->getId())->getSpecialToDate();
        // Get Current date
        if ($specialprice&& ($price > $specialprice)&&(($specialPriceFromDate <= $now && $specialPriceToDate >= $now)|| ($specialPriceFromDate <= $now && $specialPriceToDate == null)))
        {
            $output .= '<div class="label-pro-sale">SALE!</div>';
        }

        $output .= '</div>'; //#END .product-thumbnail
        $output .= '<h3 class="product-name">
                            <a href="' . $product->getProductUrl() . '">
                                ' . $product->getName() . '
                            </a>
                        </h3>';
        $productBlock = $this->getLayout()->createBlock('catalog/product_price');
        $_helper_review = $this->getLayout()->createBlock('review/helper');

        $output .= '<div class="price-box-price">'.$productBlock->getPriceHtml($product).'</div>';
        $output .= '<div class="review-box">'.$_helper_review->getSummaryHtml($product, 'short').'</div>';
        return $output;
    }

    public function renderListProduct($collection, $parameters = array()) {
        if(!isset($collection)) return '';
        //$output = '<li class="grid-sizer"></li>';
        $output = '';
        $i = 0;

        if (isset($parameters['type']) && $parameters['type'] == 'append') {
            foreach ($collection as $product) {
                if ($parameters['reload_product'] == true) {
                    $product = Mage::getModel('catalog/product')->load($product->getId());
                }
                $classes = array();
                if ($i == 0) {
                    //$classes[] = 'first';
                }
                if (intval($parameters['page-size']) > 0) {
                    $classes[] = 'col-md-' . intval(12 / $parameters['page-size']);
                }

                $output .= '<li class="item ' . implode(' ', $classes) . '">';
                $output .= KA_CustomBlock_Helper_Data::renderProductItem($product); //$this->renderProductItem($product);
                $output .= '</li>';
                $i++;
            }
        } else {
            $output = '<ul class="products row">';
            foreach ($collection as $product) {
                if ($parameters['reload_product'] == true) {
                    $product = Mage::getModel('catalog/product')->load($product->getId());
                }
                $classes = array();
                if ($i == 0) {
                    $classes[] = 'first';
                }
                if (intval($parameters['page-size']) > 0) {
                    $classes[] = 'col-md-' . intval(12 / $parameters['page-size']);
                }

                $output .= '<li class="item ' . implode(' ', $classes) . '">';
                $output .= KA_CustomBlock_Helper_Data::renderProductItem($product); //$this->renderProductItem($product);
                $output .= '</li>';
                $i++;
            }
            $output .= '</ul>';
        }

        return $output;
    }
    public function getExcludeCategories() {
        $exclude = array(56, 57, 58, 59, 60);
        if(KA_CustomBlock_Helper_Data::getSiteEVN() == 'development') {
            $exclude[] = 77;
        }
        if(KA_CustomBlock_Helper_Data::getSiteEVN() == 'stg') {
            $exclude[] = 170;
        }
        return $exclude;
    }
    public function getSiteEVN() {
        return 'live';
    }
    public function getProductByCategories($categories, $extras) {
        if(empty($categories)) return false;
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect('*')
            ->addAttributeToFilter('status', 1);
            //->addAttributeToFilter('visibility', 4);

        if(!empty($categories)) {
            for($i=0;$i<count($categories);$i++) {
                if(intval($categories[$i]) > 0) {
                    $collection->joinField('category_id_'.$i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
                    $collection->addAttributeToFilter('category_id_'.$i, array('eq' => $categories[$i]));
                }
            }
        }
//        var_dump($parameter);

        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);
        if(isset($extras['add_instock_filter']) && $extras['add_instock_filter'] == true) {
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        }
        if(isset($extras['order'])) {
            foreach($extras['order'] as $key => $type) {
                $collection->setOrder($key, $type);
            }

        }

        return $collection;
    }
}