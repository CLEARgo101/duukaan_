<?php

class KA_CustomBlock_AjaxController extends Mage_Core_Controller_Front_Action
{
	public $_ITEM_PER_PAGE = 30;
	public $_ITEM_PER_ROW = 6;

	public function indexAction()
	{

		$parameter = $this->getRequest()->getParams();
//		var_dump($parameter);
		if (isset($parameter['ajax']) && $parameter['ajax'] == 'get-product') {
			echo $this->getProduct($parameter);
		}
		if (isset($parameter['ajax']) && $parameter['ajax'] == 'featured-category') {
			echo $this->getSpecialCategoryTab($parameter);

		}
		die();
	}

	private function getProduct($parameter)
	{
		$output = '';
		//$parameter = $this->getRequest()->getParams();

		$collection = Mage::getModel('catalog/product')->getCollection();
		$collection->addAttributeToSelect('*')
				->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('special_from_date', array('date' => true, 'to' => now()))
				->addAttributeToFilter('visibility', 4);
//		$categories = null;
//		if (isset($parameter['categories']) && !empty($parameter['categories'])) {
//			$categories = explode(',', $parameter['categories']);
//		}
////        var_dump($categories);
//		if (!empty($categories)) {
//			for ($i = 0; $i < count($categories); $i++) {
//				if (intval($categories[$i]) > 0) {
//					$collection->joinField('category_id_' . $i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
//					$collection->addAttributeToFilter('category_id_' . $i, array('eq' => $categories[$i]));
//				}
//			}
//		}

		$collection->addFinalPrice();
		$collection->getSelect()->where('price_index.final_price < price_index.price');
        $collection->getSelect()->order('special_from_date DESC');
        if(isset($parameter['type']) && $parameter['type'] == 'append')
        {
            $collection->setPage(37,100);
        }
        else{
            $collection->setPageSize(36);
        }
		if (intval($parameter['page-size']) > 0) {
			$collection->setPageSize($parameter['page-size']);
		}
//        var_dump($parameter);

		Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
		//$collection->setPageSize($this->_ITEM_PER_PAGE);
		$_TOTAL = $collection->getSize();

		if ($_TOTAL == 0) {
			$output = '<p class="message empty">There isn\'t any product in this category</p>';
		} else {

			/*
			 * Block show end time in LATEST DEALS
			 *
			$dateTime = new DateTime();
			$now = $dateTime;
			foreach ($collection as $product) {
				$product = Mage::getModel('catalog/product')->load($product->getId());
				$endDateSpecial = $product->getSpecialToDate();
//				var_dump(($endDateSpecial));
//				var_dump(($dateTime));
				if (strtotime($endDateSpecial) > strtotime($dateTime) && $endDateSpecial != null) {
					$dateTime = new DateTime($endDateSpecial);
				}
			}

			$diff = $dateTime->diff($now);

			$output .= '<div class="special-end-in">END IN ';
			$output .= '<div class="special-end-in-item days">'.$diff->days.' Days</div>';
			$output .= '<div class="special-end-in-item hours">'.$diff->h.' Hrs</div>';
			$output .= '<div class="special-end-in-item minutes">'.$diff->i.' Mins</div>';
			$output .= '<div class="special-end-in-item seconds">'.$diff->s.' Secs</div>';
			$output .= '</div>';*/

			$output .= KA_CustomBlock_Helper_Data::renderListProduct($collection, $parameter);
		}
		return $output;
	}

	private function getProductByType($parameter)
	{
		$output = '';
		//$parameter = $this->getRequest()->getParams();
		$collection = Mage::getModel('catalog/product')->getCollection();
		$collection->addAttributeToSelect('*')
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', 4);
		$categories = null;
		if (isset($parameter['categories']) && !empty($parameter['categories'])) {
			$categories = explode(',', $parameter['categories']);
		}
		if (!empty($categories)) {
			for ($i = 0; $i < count($categories); $i++) {
//				var_dump($categories[$i]);
				if ($i == 1) {

					/*if ($categories[$i] == 'best-seller') {
						$select = Mage::getSingleton('core/resource')->getConnection('core_read')
								->select()
								->from('sales_flat_order_item', array('product_id', 'count' => 'SUM(`qty_ordered`)'))
								->group('product_id');

						$collection->getSelect()
								->join(array('bs' => $select),
										'bs.product_id = e.entity_id')
								->order('bs.count DESC');
					} else if ($categories[$i] == 'special') {
						$collection->addFinalPrice();
						$collection->getSelect()->where('price_index.final_price < price_index.price');
					} else if ($categories[$i] == 'new-arrivals') {
						$collection->addAttributeToSort("entity_id", "DESC");
					} else if ($categories[$i] == 'most-review') {
						$collection->joinField('reviews_count', 'review_entity_summary', 'reviews_count', 'entity_pk_value=entity_id', array('entity_type'=>1, 'store_id'=> Mage::app()->getStore()->getId()), 'left')
								->getSelect()->where('at_reviews_count.reviews_count IS NOT NULL')->order('at_reviews_count.reviews_count DESC');
					}*/

					if ($categories[$i] == 'best-seller') {
						$collection->joinField('category_id_' . $i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
						$collection->addAttributeToFilter('category_id_' . $i, array('eq' => 57));
					} else if ($categories[$i] == 'special') {
						$collection->joinField('category_id_' . $i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
						$collection->addAttributeToFilter('category_id_' . $i, array('eq' => 58));
					} else if ($categories[$i] == 'new-arrivals') {
						$collection->joinField('category_id_' . $i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
						$collection->addAttributeToFilter('category_id_' . $i, array('eq' => 59));
					} else if ($categories[$i] == 'most-review') {
						$collection->joinField('category_id_' . $i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
						$collection->addAttributeToFilter('category_id_' . $i, array('eq' => 60));
					}
				} else {
					require_once 'KA/MobileDetect/Mobile_Detect.php';
					$detect = new Mobile_Detect;
					if ($detect->isMobile())
					{
						$parent_category = Mage::getModel('catalog/category')
								->load($categories[$i])->getParentCategory();
						$subcats = $parent_category->getChildren();
						$array_list = array();
						foreach(explode(',',$subcats) as $subCatid)
						{
							$_sub_category = Mage::getModel('catalog/category')->load($subCatid);
							if($_sub_category->getIsActive() && 233 == $_sub_category->getShowOnFrontPage())
							{
								$array_list[] = $_sub_category->getId();
//								var_dump($_sub_category->getId());
							}
						}
						if(count($array_list)>0)
						{
							$collection->joinField('category_id_' . $i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
							$collection->addAttributeToFilter('category_id_' . $i, array('in' => $array_list));
						}
					}
					else
					{
						$collection->joinField('category_id_' . $i, 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left');
						$collection->addAttributeToFilter('category_id_' . $i, array('eq' => $categories[$i]));
					}
				}
			}
		}

		if ($detect->isMobile()) {
			$collection->setPageSize(12);
		}
		else{
			if (intval($parameter['page-size']) > 0) {
				$collection->setPageSize($parameter['page-size']);
			}
		}
//        var_dump($parameter);
		Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
		//$collection->setPageSize($this->_ITEM_PER_PAGE);
		$_TOTAL = $collection->getSize();
		if ($_TOTAL == 0) {
			$output = '<p class="message empty">There isn\'t any product in this category</p>';
		} else {
			$output .= KA_CustomBlock_Helper_Data::renderListProduct($collection, $parameter);
		}
		return $output;

	}

//fix git conflict
	private function getSpecialCategoryTab($parameter)
	{
		$categories = Mage::getModel('catalog/category')
				->getCollection()
				->addAttributeToSelect('*')
				->addAttributeToFilter('show_as_featured_category_tab', array('eq' => 235))
				->addIsActiveFilter();
		$_selected_categories = array();
		if ($parameter['categories']) {
			$_selected_categories = explode(',', $parameter['categories']);
		}
		if (!empty($categories)) {
			$output = '<ul class="featured-categories-special-tabs">';
			$i = 0;
			$_category_tab_first = 0;

			foreach ($categories as $category) {
				$_classes = '';
				if (count($_selected_categories) > 1) {
					if (in_array($category->getUrlKey(), $_selected_categories)) {
						$_classes = 'active';
					}
				} elseif ($i == 0) {
					$_classes = 'active';
					$_category_tab_first = $category;

					//var_dump($i);
				}
				//var_dump($category->getName());
				$output .= '<li class="featured-category-tab ' . $_classes . '" data-id="' . $category->getUrlKey() . '">' . $category->getName() . '</li>';
				$i++;
			}


			$output .= '</ul>';
			$output .= '<button type="button" class="show-more-categories-featured"></button>';
			if (intval($_category_tab_first) > 0) {
				$parameter['categories'] = ($parameter['categories'] != '') ?
						$parameter['categories'] . ',' . $_category_tab_first->getUrlKey() : $_category_tab_first->getUrlKey();
			}

			$output .= '<div class="categories-featured-product-wrapper">';
			$_category_to_get_image = Mage::getModel('catalog/category')->load($_selected_categories[0]);
			$_helper = Mage::helper('catalog/output');
			//$_category  = Mage::getModel('catalog/category')->load(CATEGORYID);// get your category here.
			$_imgHtml = '';
			if ($_imgUrl = $_category_to_get_image->getImageUrl()) {
				$_imgHtml = '<img src="' . $_imgUrl . '" alt="' . $_helper->htmlEscape($_category_to_get_image->getName()) . '" title="' . $_helper->htmlEscape($_category_to_get_image->getName()) . '" />';
				$_imgHtml = $_helper->categoryAttribute($_category_to_get_image, $_imgHtml, 'image');
				//echo $_imgHtml;
				$_imgHtml = '<div class="category-thumbnail">' . $_imgHtml . '</div>';
			}
			$output .= $_imgHtml;
			//var_dump($parameter['categories']);
			$output .= $this->getProductByType($parameter);
			$output .= '</div>';
		}

		return $output;
	}
}