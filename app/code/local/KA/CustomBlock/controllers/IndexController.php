<?php
class KA_CustomBlock_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {
        //var_dump(1);
    }
    public function sellonduukaanAction ()
    {
        // Create a generic template block
        $block = $this->getLayout()->createBlock('core/template');
        $block->setCacheLifetime(null);
        // Assign your template to it
        // This path is relative to your current theme (eg: rwd/default/template/...)
        $block->setTemplate('custom_blocks/merchant_home_tabs.phtml');
        $messages=Mage::getSingleton("checkout/session")->getMessages(true)->getItems();
        //var_dump($messages);
        if(isset($messages)) {
            foreach ($messages as $message) {
                //var_dump($message->getText());
                Mage::getSingleton('core/session')->addError($message->getText());
            }
        }


        $this->loadLayout();


        //$block_message = $this->getLayout()->createBlock("core/messages")->setMessages($messages);

        //$this->getLayout()->getBlock('content')->append($block_message);
        $this->getLayout()->getBlock('content')->append($block);
        // Render the template to the browser
//        echo $block->toHtml();
        $root = $this->getLayout()->getBlock('root');
        $root->addBodyClass('cms-sell-on-duukaan');
        $root->setTemplate('page/1column.phtml');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Sell On DuuKaan'));

        $this->renderLayout();
    }
}