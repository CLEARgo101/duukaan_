<?php

class KA_CustomAjax_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$parameter = $this->getRequest()->getParams();
		$session = Mage::getSingleton("core/session");
		if (isset($parameter['type']) && $parameter['type'] == 'pickup') {
			$session->setData("typeShip", 'pickup');
		}
		else{
			$session->unsetData('typeShip');
		}
	}

	public function importAction()
	{
		$parameter = $this->getRequest()->getParams();
		var_dump($parameter);
	}
}