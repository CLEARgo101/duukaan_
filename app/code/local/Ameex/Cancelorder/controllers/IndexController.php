<?php

class Ameex_Cancelorder_IndexController extends Mage_Core_Controller_Front_Action
{
	public function cancelAction()
	{
		$params = $this->getRequest()->getParams();
		$orderId = $params['id'];
		$order = Mage::getModel('sales/order')->load($orderId);

		if($order->getStatus() == 'pending')
		{
			$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
			Mage::getSingleton('core/session')->setSuccessmsg('<div class="success-msg">your order has been canceled</div>');


			$emailTemplate = Mage::getModel('core/email_template')->loadDefault('sales_email_order_cancellation_template');

			$emailTemplateVariables = array(
					'order_increment_id' => $order->getIncrementId(),
					'store_email' => Mage::getStoreConfig('trans_email/ident_general/email'),
					'store_phone' => Mage::getStoreConfig('general/store_information/phone'),
			);


			$emailTemplate->setSenderName('Duukaan Admin');
			$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
			$emailTemplate->setTemplateSubject("Cancellation Order #".$order->getIncrementId());
			$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);

			$emailTemplate->send(Mage::getStoreConfig('trans_email/ident_general/email'), 'Duukaan Admin', $emailTemplateVariables);

			$customer = Mage::getSingleton('customer/session')->getCustomer();
			$customerData = Mage::getModel('customer/customer')->load($customer->getId())->getData();

			$emailTemplate->send($customerData['email'], $customerData['firstname'].' '.$customerData['lastname'], $emailTemplateVariables);

			$this->_redirectReferer();
		}
		else if($order->getStatus() != 'complete' || $order->getStatus() != 'closed' || $order->getStatus() != 'canceled')
		{
			$order->setStatus(Mage_Sales_Model_Order::STATE_WAITING_REFUND, true)->save();

			$emailTemplate = Mage::getModel('core/email_template')->loadDefault('sales_email_order_cancellation_template');

			$emailTemplateVariables = array(
					'order_increment_id' => $order->getIncrementId(),
					'store_email' => Mage::getStoreConfig('trans_email/ident_general/email'),
					'store_phone' => Mage::getStoreConfig('general/store_information/phone'),
			);


			$emailTemplate->setSenderName('Duukaan Admin');
			$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
			$emailTemplate->setTemplateSubject("Cancellation Order #".$order->getIncrementId());
			$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);

			$emailTemplate->send(Mage::getStoreConfig('trans_email/ident_general/email'), 'Duukaan Admin', $emailTemplateVariables);

			$customer = Mage::getSingleton('customer/session')->getCustomer();
			$customerData = Mage::getModel('customer/customer')->load($customer->getId())->getData();

			$emailTemplate->send($customerData['email'], $customerData['firstname'].' '.$customerData['lastname'], $emailTemplateVariables);

			$this->_redirectReferer();
		}
	}
}