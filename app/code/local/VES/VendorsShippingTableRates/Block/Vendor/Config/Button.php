<?php
class VES_VendorsShippingTableRates_Block_Vendor_Config_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    /**
     * Get the grid and scripts contents
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $html = $this->_toHtml();
        $this->_arrayRowsCache = null; // doh, the object is used as singleton!
        return $html;
    }
    
	/**
     * Check if columns are defined, set template
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('ves_vendorsshippingtablerates/export.phtml');
    }
    
    public function getExportUrl(){
        return $this->getUrl('vendors/tablerates/export');
    }
}