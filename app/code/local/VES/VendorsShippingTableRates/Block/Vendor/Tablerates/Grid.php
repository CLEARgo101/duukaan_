<?php

class VES_VendorsShippingTableRates_Block_Vendor_Tablerates_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('vendorsTableRates');
      $this->setDefaultSort('pk');
      $this->setDefaultDir('ASC');
  }

  protected function _prepareCollection()
  {

  	  $vendorId = Mage::getSingleton('vendors/session')->getVendorId();
      $collection = Mage::getModel('vendorstablerates/tablerate')->getCollection()
        ->addFieldToFilter('condition_name',$this->getCondition())
        ->addFieldToFilter('vendor_id',$vendorId)
        ;
      $this->setCollection($collection);

      return parent::_prepareCollection();
  }

    protected function _prepareColumns()
    {
       $this->addColumn('dest_country_id', array(
            'header'    => $this->__('Country'),
        	'type'		=> 'text',
            'index'     => 'dest_country_id',
        ));
       
    	$this->addColumn('dest_region_id', array(
            'header'    => $this->__('Region/State'),
    	    'index'     => 'dest_region_code',
        ));
        
    	$this->addColumn('dest_zip', array(
    	    'header'    => $this->__('Zip/Postal Code'),
    	    'index'     => 'dest_zip',
    	));
    	
    	$conditionNameAbove = $this->__('Weight >=');
    	$conditionNameLess = $this->__('Weight <=');
    	switch($this->getCondition()){
    	    case VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_PRICE_DESTINATION:
    	        $conditionNameAbove = $this->__('Order Subtotal >=');
    	        $conditionNameLess = $this->__('Order Subtotal <=');
    	        break;
	        case VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_QTY_DESTINATION:
	            $conditionNameAbove = $this->__('# of Items >=');
	            $conditionNameLess = $this->__('# of Items <=');
	            break;
            case VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_WEIGHT_DESTINATION:
               $conditionNameAbove = $this->__('Weight >=');
    	       $conditionNameLess = $this->__('Weight <=');
                break;
    	}

    	$this->addColumn('condition_value_from', array(
    	    'header'    => $conditionNameAbove,
    	    'index'     => 'condition_value_from',
    	));

    	$this->addColumn('condition_value_to', array(
    	    'header'    => $conditionNameLess,
    	    'index'     => 'condition_value_to',
    	));
    	$this->addColumn('price', array(
    	    'header'    => $this->__('Shipping Price'),
    	    'index'     => 'price',
    	));
    	$this->addColumn('delivery_type', array(
    	    'header'    => $this->__('Delivery Type'),
    	    'index'     => 'delivery_type',
    	));
      return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        return $this;
    }
}