<?php
class VES_VendorsShippingTableRates_Model_Resource_Tablerate_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_regions;
    
	protected function _construct()
    {
        $this->_init('vendorstablerates/tablerate');
    }
    /**
     * Init regions
     * @return VES_VendorsShippingTableRates_Model_Resource_Tablerate_Collection
     */
    protected function _initRegions(){
        $collection = Mage::getResourceModel('directory/region_collection');
        foreach ($collection->getData() as $row) {
            $this->_regions[$row['country_id']][$row['region_id']] =$row['code'];
        }
        return $this;
    }
    
    /**
     * Set the region code to the collection
     * @see Mage_Core_Model_Resource_Db_Collection_Abstract::_afterLoad()
     */
    protected function _afterLoad(){
        parent::_afterLoad();
        $this->_initRegions();

        foreach ($this->_items as $item) {
            if($item->getData('dest_region_id') === '0' || $item->getData('dest_region_id') === ''){
                $item->setData('dest_region_code','*');
            }else{
                $regionCode = isset($this->_regions[$item->getData('dest_country_id')][$item->getData('dest_region_id')])?$this->_regions[$item->getData('dest_country_id')][$item->getData('dest_region_id')]:'*';
                $item->setData('dest_region_code',$regionCode);
            }
        }
        return $this;
    }
}