<?php

class VES_VendorsShippingTableRates_Model_Carrier_Tablerates
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    CONST CONDITION_WEIGHT_DESTINATION      = 'package_weight';
    CONST CONDITION_PRICE_DESTINATION       = 'package_value';
    CONST CONDITION_QTY_DESTINATION         = 'package_qty';
    
    protected $_code = 'vendor_tablerates';
    protected $_isFixed = true;

    
    /**
     * Get the method title of vendor
     * @param int $vendorId
     */
    public function getMethodTitle($vendorId){
        return Mage::helper('vendorsconfig')->getVendorConfig('shipping/vendor_tablerates/title',$vendorId);
    }
    
    /**
     * get if the method is enabled for the vendor
     * @param int $vendorId
     * @return boolean 
     */
    public function isEnabledMethod($vendorId){
        return Mage::helper('vendorsconfig')->getVendorConfig('shipping/vendor_tablerates/active',$vendorId);
    }
    /**
     * Group Items by vendor
     * @return Ambigous <multitype:multitype: , unknown>
     */
    public function groupItemsByVendor($request){
        $quotes = array();
        foreach($request->getAllItems() as $item) {
            if($item->getParentItem()) continue;
            if($item->getProduct()->getVendorId()) {
                
                if($item->getVendorId()){
                    $vendorId = $item->getVendorId();
                }else{
                    $vendorId = $item->getProduct()->getVendorId();
                }
                
                $transport = new Varien_Object(array('vendor_id'=>$vendorId,'item'=>$item));
                Mage::dispatchEvent('ves_vendors_checkout_init_vendor_id',array('transport'=>$transport));
                $vendorId = $transport->getVendorId();
                /*Get item by vendor id*/
                if(!isset($quotes[$vendorId])) $quotes[$vendorId] = array();
                $quotes[$vendorId][] = $item;
            } else {
                $quotes['no_vendor'][] = $item;
            }
        }
        return $quotes;
    }
    
    /**
     * Get Rate
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @param array $items
     * @param Varien_Object $additionalRequest
     *
     * @return Mage_Core_Model_Abstract
     */
    public function getRate(Mage_Shipping_Model_Rate_Request $request,$items=array(), Varien_Object $additionalRequest)
    {
        return Mage::getResourceModel('vendorstablerates/tablerate')->getRate($request,$items,$additionalRequest);
    }
    /**
     * Format Key for URL
     *
     * @param string $str
     * @return string
     */
    public function formatMethodName($str)
    {
        $methodName = preg_replace('#[^0-9a-z]+#i', '_', Mage::helper('catalog/product_url')->format($str));
        $methodName = strtolower($methodName);
        $methodName = trim($methodName, '_');
    
        return $methodName;
    }
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        

        $quotes = $this->groupItemsByVendor($request);
        $vendorRates = array();
        $result = Mage::getModel('shipping/rate_result');
        
        /*
         * seperate each vendor item to dependence array in quotes array.
         */
        $desCountryId = $request->getDestCountryId();
        foreach($quotes as $vendorId=>$items){
            if(!$this->isEnabledMethod($vendorId)) continue;

            /*================================================*/
            $vendor = Mage::getModel('vendors/vendor')->load($vendorId);
            
            $condition = Mage::helper('vendorsconfig')->getVendorConfig('shipping/vendor_tablerates/condition',$vendorId);
            $conditionValue = 0;

            switch ($condition){
                case self::CONDITION_WEIGHT_DESTINATION:
                    $weight = 0;
                    foreach($items as $item){
                        $weight+= $item->getRowWeight();
                    }
                    $weight = $this->getTotalNumOfBoxes($weight);
                    $conditionValue = $weight;
                    break;
                case self::CONDITION_PRICE_DESTINATION:
                    $rowTotal = 0;
                    foreach($items as $item){
                        $rowTotal+= $item->getRowTotal();
                    }
                    $conditionValue = $rowTotal;
                    break;
                case self::CONDITION_QTY_DESTINATION:
                    $totalQty = 0;
                    foreach($items as $item){
                        $totalQty+= $item->getQty();
                    }
                    $conditionValue = $totalQty;
                    break;
            }

            $additionalRequest = new Varien_Object(array(
                'condition_name'    => $condition,
                'condition_value'   => $conditionValue,
                'vendor_id'         => $vendorId,
            ));
            
            $rates = $this->getRate($request,$items,$additionalRequest);
            
            if (!empty($rates) && is_array($rates)) {
                foreach($rates as $rate){
                    $method = Mage::getModel('shipping/rate_result_method');
        
                    $method->setCarrier('vendor_tablerates');
                    $method->setCarrierTitle($this->getConfigData('title'));
                    $method->setMethod($this->formatMethodName($rate['delivery_type']).$rate['pk'].VES_VendorsShipping_Model_Shipping::DELEMITER.$vendorId);
                    
                    $method->setMethodTitle($rate['delivery_type']);
        
                    if ($request->getFreeShipping() === true) {
                        $shippingPrice = 0;
                    } else {
                        /*shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);*/
                        $shippingPrice = $rate['price'];
                    }

                    $method->setPrice($shippingPrice);
                    
                    if( !empty($rate['cost']) ) {
                        $method->setCost($rate['cost']);
                    }
                    
                    $method->setVendorId($vendorId);
                    $result->append($method);
                }
            }
        }
        return $result;
    }


    public function updateShippingPrice($quote) {
        $total = 0;
        $_isFreeShipping = false;
        foreach($quote as $item) {
            if($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }
            if($item->getFreeShipping()) continue;
            $total += $item->getRowTotal()*$item->getQty();
        }

        if($total >= $this->getFreeShippingSubtotal() && $this->getFreeShippingSubtotal() != '') $_isFreeShipping = true;
        return $_isFreeShipping;
    }

    public function getAllowedMethods()
    {
        return array('vendor_flatrate'=>$this->getConfigData('name'));
    }
}
