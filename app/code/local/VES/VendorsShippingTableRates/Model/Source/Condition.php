<?php
class VES_VendorsShippingTableRates_Model_Source_Condition
{
	public function toOptionArray() {
 		 $data=array(
 		     array(
 		         'label' => Mage::helper('vendorstablerates')->__('Weight vs. Destination'),
 		         'value' => VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_WEIGHT_DESTINATION
 		     ),
 		     array(
 		         'label' => Mage::helper('vendorstablerates')->__('Price vs. Destination'),
 		         'value' => VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_PRICE_DESTINATION
 		     ),
 		     array(
 		         'label' => Mage::helper('vendorstablerates')->__('# of Items vs. Destination'),
 		         'value' => VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_QTY_DESTINATION
 		     ),
 		 );
         return $data;
	} 
	
	public function getOptionArray() {
 		 return array(
 		     VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_WEIGHT_DESTINATION => Mage::helper('vendorstablerates')->__('Weight vs. Destination'),
 		     VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_PRICE_DESTINATION => Mage::helper('vendorstablerates')->__('Price vs. Destination'),
 		     VES_VendorsShippingTableRates_Model_Carrier_Tablerates::CONDITION_QTY_DESTINATION => Mage::helper('vendorstablerates')->__('# of Items vs. Destination'),
 		 );
	}
}