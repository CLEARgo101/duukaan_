<?php

$installer = $this;

$table = $installer->getConnection()
    ->newTable($installer->getTable('vendorstablerates/tablerate'))
    ->addColumn('pk', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('vendor_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Vendor Id')
    ->addColumn('dest_country_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 4, array(
        'nullable'  => false,
        ), 'Destination country id')
    ->addColumn('dest_region_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        ), 'Destination region id')
    ->addColumn('dest_zip', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable'  => false,
        ), 'Destination zip code')
    ->addColumn('condition_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
        'nullable'  => false,
        ), 'Condition Name')
    ->addColumn('condition_value_from', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        'nullable'  => false,
        ), 'Condition Value')
    ->addColumn('condition_value_to', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
            'nullable'  => false,
        ), 'Condition Value')
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
            'nullable'  => false,
        ), 'Shipping Price')
    ->addColumn('delivery_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => true,
        ), 'Delivery Type')
        ;
$installer->getConnection()->createTable($table);

$installer->endSetup(); 