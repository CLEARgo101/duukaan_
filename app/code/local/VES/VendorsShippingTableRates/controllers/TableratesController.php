<?php
class VES_VendorsShippingTableRates_TableratesController extends VES_Vendors_Controller_Action
{
	public function exportAction(){
	    $fileName   = 'tablerates.csv';
	    $defaultCondition = $this->getRequest()->getParam('condition');
	    $content    = $this->getLayout()->createBlock('vendorstablerates/vendor_tablerates_grid')->setCondition($defaultCondition)
	    ->getCsv();  
	    $this->_sendUploadResponse($fileName, $content);
	}
	
	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
	{
	    $response = $this->getResponse();
	    $response->setHeader('HTTP/1.1 200 OK','');
	    $response->setHeader('Pragma', 'public', true);
	    $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
	    $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
	    $response->setHeader('Last-Modified', date('r'));
	    $response->setHeader('Accept-Ranges', 'bytes');
	    $response->setHeader('Content-Length', strlen($content));
	    $response->setHeader('Content-type', $contentType);
	    $response->setBody($content);
	    $response->sendResponse();
	    die;
	}
}