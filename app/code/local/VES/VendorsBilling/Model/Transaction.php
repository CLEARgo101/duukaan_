<?php

class VES_VendorsBilling_Model_Transaction extends Mage_Core_Model_Abstract
{

	public function _construct()
    {
        parent::_construct();
        $this->_init('vendorsbilling/transaction');
    }
    
	protected function _afterSave()
    {
    	/*Send Notification email to vendor*/
    	Mage::helper('vendorsbilling')->sendCreditBalanceChangeNotificationEmail($this);
        return parent::_afterSave();
    }
}