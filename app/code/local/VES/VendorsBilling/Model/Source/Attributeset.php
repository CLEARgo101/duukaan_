<?php

class VES_VendorsBilling_Model_Source_Attributeset
{
    public function toOptionArray(){
        $entityTypeId = Mage::getModel('eav/entity')
            ->setType('catalog_product')
            ->getTypeId();
        $attributeSetCollection = Mage::getModel('eav/entity_attribute_set')
            ->getCollection()
            ->setEntityTypeFilter($entityTypeId);
        $options = array();
        foreach($attributeSetCollection as $_attributeSet){
            $options[] = array('label' => Mage::helper('vendorsbilling')->__($_attributeSet->getAttributeSetName()),
                                'value' => $_attributeSet->getAttributeSetId());
        }

        return $options;
    }
}