<?php
class VES_VendorsBilling_Model_Type_Billing extends VES_VendorsBilling_Model_Type_Abstract
{
    public function process(){
        $related_vendor_id		= $this->getRelatedVendorId();
        $vendor 		        = Mage::getModel('vendors/vendor')->load($related_vendor_id);
        $type			        = $this->getType();
        $amount			        = $this->getAmount();
        $note			        = $this->getData('description');
        $description	        = $note?$note:Mage::helper('vendorsbilling')->__('You made a payment');
        $action 		        = $this->getAction();
        $orderItem				= $this->getOrderItem();
        $invoice				= $this->getInvoice();
        
        $additionalInfo			= 'order_item|'.$orderItem->getId().'||invoice|'.$invoice->getId();
        /*Do nothing if the amount is zero*/
        if(!$amount) return;

        $result = $this->processAmount($vendor, $action, $amount);

        /*Save transaction*/
        $transaction = Mage::getModel('vendorsbilling/transaction')->setData(array(
            'vendor_id'			=> $related_vendor_id,
            'type'				=> $type,
            'amount'			=> $amount,
            'fee'				=> 0,
            'net_amount'		=> $amount,
            'balance'			=> $vendor->getVesBillingVendor(),
            'description'		=> $description,
            'additional_info'	=> $additionalInfo,
            'created_at'		=> now(),
        ))->save();
    }

    public function getDescription(VES_VendorsBilling_Model_Transaction $transaction){
        return Mage::helper('vendorsbilling')->__('You made a payment');
    }
}