<?php
class VES_VendorsBilling_Block_Dashboard extends Mage_Core_Block_Template
{
	public function getVendor(){
		return Mage::getSingleton('vendors/session')->getVendor();
	}
	
	public function getCredit(){
		return Mage::helper('vendorsbilling')->formatCredit($this->getVendor()->getCredit());
	}

    public function getBilling(){
        return Mage::helper('vendorsbilling')->formatBilling($this->getVendor()->getVesBillingVendor());
    }
}