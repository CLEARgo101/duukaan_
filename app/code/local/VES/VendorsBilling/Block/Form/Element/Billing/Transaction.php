<?php
class VES_VendorsBilling_Block_Form_Element_Billing_Transaction
    extends Mage_Adminhtml_Block_Widget_Grid implements Varien_Data_Form_Element_Renderer_Interface
{
    /**
     * Form element instance
     *
     * @var Varien_Data_Form_Element_Abstract
     */
    protected $_element;

    /**
     * Render HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->getLayout()->createBlock('vendorsbilling/transaction_grid')->toHtml();
    }

    /**
     * Set form element instance
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Price_Group_Abstract
     */
    public function setElement(Varien_Data_Form_Element_Abstract $element)
    {
        $this->_element = $element;
        return $this;
    }

    /**
     * Retrieve form element instance
     *
     * @return Varien_Data_Form_Element_Abstract
     */
    public function getElement()
    {
        return $this->_element;
    }

    /**
     * Initialize block
     */
    public function __construct()
    {
        parent::__construct();
        $this->setType('note');
    }

    /**
     * Prepare global layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }
}
