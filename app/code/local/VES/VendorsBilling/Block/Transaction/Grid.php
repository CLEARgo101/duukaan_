<?php

class VES_VendorsBilling_Block_Transaction_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('vendorsTransactionGrid');
      $this->setDefaultSort('created_at');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
      $this->setUseAjax(false);
  }

  protected function _prepareCollection()
  {
  	  $vendorId = Mage::getSingleton('vendors/session')->getVendorId();
      $collection = Mage::getModel('vendorsbilling/transaction')->getCollection()->addFieldToFilter('vendor_id',$vendorId);
      $this->setCollection($collection);

      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('created_at', array(
            'header'    => $this->__('Created At'),
        	'type'		=> 'datetime',
      		'width'		=> '160px',
            'index'     => 'created_at',
        ));
    	$this->addColumn('description', array(
            'header'    => $this->__('Description'),
            'index'     => 'description',
    		'renderer'	=> new VES_VendorsBilling_Block_Widget_Grid_Column_Renderer_Description(),
    		'filter'	=> false,
    		'sortable'	=> false,
        ));
        
		$baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();

        $this->addColumn('amount', array(
            'header'    => $this->__('Debits'),
            'align'     => 'right',
            'type'      => 'currency',
        	'currency_code'  => $baseCurrencyCode,
        	'renderer'	=> new VES_VendorsBilling_Block_Widget_Grid_Column_Renderer_Amount(),
            'index'     => 'amount'
        ));

        $this->addColumn('balance', array(
            'header'    => $this->__('Balance'),
            'align'     => 'right',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
            'index'     => 'balance'
        ));
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        return $this;
    }

//  public function getRowUrl($row)
//  {
//      return $this->getUrl('*/*/view', array('id' => $row->getId()));
//  }

}