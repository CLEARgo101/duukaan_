<?php

class VES_VendorsBilling_Block_Billing_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('billing_form', array('legend'=>Mage::helper('vendorsbilling')->__('Billing Information'), 'class'=>'fieldset-wide'));

      $fieldset->addType('billing_info','VES_VendorsBilling_Block_Form_Element_Billing_Info');
      $fieldset->addField('ves_billing_vendor', 'billing_info', array(
          'label'     => Mage::helper('vendors')->__('Billing'),
          'name'      => 'ves_billing_vendor',
      ));

      $fieldset1 = $form->addFieldset('transaction_set', array('legend'=>Mage::helper('vendorsbilling')->__('Transaction'), 'class'=>'fieldset-wide'));


      $fieldset1->addField('transaction', 'text', array(
          'name'=>'transaction',
          'label'   => Mage::helper('vendors')->__('Transaction'),
      ));

      $form->getElement('transaction')->setRenderer(
          $this->getLayout()->createBlock('vendorsbilling/form_element_billing_transaction')
      );

      if ( Mage::getSingleton('adminhtml/session')->getVendorsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getVendorsData());
          Mage::getSingleton('adminhtml/session')->setVendorsData(null);
      } elseif ( Mage::registry('vendors_data') ) {
          $form->setValues(Mage::registry('vendors_data')->getData());
      }

      return parent::_prepareForm();
  }
}