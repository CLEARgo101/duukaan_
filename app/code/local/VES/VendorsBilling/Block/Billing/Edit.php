<?php

class VES_VendorsBilling_Block_Billing_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'vendorsbilling';
        $this->_controller = 'billing';
        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('save');
        $this->_addButton('billing',array(
            'label' =>  'Make a payment',
            'onclick'   => "setLocation('". $this->getUrl('vendors/credit_billing/billing') ."');"
        ));
    }



    public function getHeaderText()
    {
    	return Mage::helper('vendorsbilling')->__('Billing Information');
    }
    public function getBackUrl(){
    	return $this->getUrl('vendors');
    }
}