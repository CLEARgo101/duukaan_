<?php

class VES_VendorsBilling_Block_Adminhtml_Vendor_Transaction_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'vendorsbilling';
        $this->_controller = 'adminhtml_vendor_transaction';
        
        $this->_updateButton('save', 'label', Mage::helper('vendorsbilling')->__('Save Transaction'));
        $this->_removeButton('reset');
    }

    public function getHeaderText()
    {
    	return Mage::helper('vendorsbilling')->__('Add Transaction');
    }
}