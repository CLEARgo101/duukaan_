<?php

class VES_VendorsBilling_Block_Adminhtml_Vendor_Grid extends Mage_Adminhtml_Block_Template{
	protected function _prepareLayout(){
		parent::_prepareLayout();
		$vendorGridContainer = $this->getLayout()->getBlock('vendors');
		if($vendorGridContainer){
			$grid = $vendorGridContainer->getChild('grid');
			$baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
			$grid->addColumnAfter('billing', array(
				'header'    => Mage::helper('vendorsbilling')->__('Billing'),
				'align'     =>'right',
				'width'     => '100px',
				'index'     => 'ves_billing_vendor',
				'type'      => 'currency',
				'currency_code'  => $baseCurrencyCode,
				'renderer'  => new VES_VendorsBilling_Block_Widget_Grid_Column_Renderer_Billing(),
			),'email');
		}
		return $this;
	}
}