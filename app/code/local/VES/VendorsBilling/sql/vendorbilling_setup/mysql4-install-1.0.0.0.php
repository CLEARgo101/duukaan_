<?php

$installer = $this;
/**
 * Create table 'vendor_transaction'
 */

$table = $installer->getConnection()
    ->newTable($installer->getTable('vendorsbilling/transaction'))
    ->addColumn('transaction_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Transaction Id')
    ->addColumn('vendor_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Vendor Id')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'nullable'  => false,
        ), 'Transaction Type')
    ->addColumn('amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        'nullable'  => false,
        ), 'Transaction amount')
    ->addColumn('fee', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        'nullable'  => false,
        ), 'Fee')
    ->addColumn('net_amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        'nullable'  => false,
        ), 'Net amount')
    ->addColumn('balance', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12,4), array(
        'nullable'  => false,
        ), 'Balance amount')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, Varien_Db_Ddl_Table::MAX_TEXT_SIZE, array(
        'nullable'  => true,
        ), 'Transaction description')
    ->addColumn('additional_info', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        ), 'Additional info')
   	->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
        ), 'Created At')
        ;
$installer->getConnection()->createTable($table);

$this->addAttribute('ves_vendor', 'ves_billing_vendor', array(
        'type'              => 'static',
        'label'             => 'Billing Amount',
        'input'             => 'text',
        'class'             => '',
        'backend'           => '',
        'frontend'          => '',
        'source'            => '',
        'required'          => true,
        'user_defined'      => false,
        'default'           => 0,
        'unique'            => false,
));
$this->getConnection()->addColumn($this->getTable('vendors/vendor'), 'ves_billing_vendor', 'varchar(255) NOT NULL DEFAULT 0 AFTER vendor_id');

$catalogEavSetup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup();
$catalogEavSetup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'is_billing_vendor', array(
    'group'                                         => 'General',
    'sort_order'                                    => 99,
    'type'                                          => 'int',
    'backend'                                       => '',
    'frontend'                                      => '',
    'label'                                         => 'Is billing vendor',
    'note'                                          => '',
    'input'                                         => 'select',
    'class'                                         => '',
    'source'                                        => 'eav/entity_attribute_source_boolean',
    'global'                                        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                                       => true,
    'required'                                      => false,
    'user_defined'                                  => false,
    'default'                                       => '1',
    'visible_on_front'                              => false,
    'unique'                                        => false,
    'is_configurable'                               => false,
    'used_for_promo_rules'                          => false,
    'apply_to'                                      => 'virtual',
));

$catalogEavSetup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'related_vendor_id', array(
    'group' 						=> 'General',
    'sort_order' 					=> 100,
    'type' 							=> 'text',
    'backend' 						=> '',
    'frontend' 						=> '',
    'label' 						=> 'Related vendor ID',
    'note' 							=> '',
    'input' 						=> 'text',
    'class' 						=> '',
    'source' 						=> '',
    'global' 						=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' 						=> true,
    'required' 						=> false,
    'user_defined' 					=> false,
    'default' 						=> '',
    'visible_on_front' 				=> false,
    'unique' 						=> false,
    'is_configurable' 				=> false,
    'used_for_promo_rules' 			=> false,
    'searchable'        			=> true,
    'filterable'        			=> false,
    'comparable'        			=> true,
    'visible_in_advanced_search' 	=> true,
    'apply_to'                      => 'virtual',
));

$installer->endSetup(); 