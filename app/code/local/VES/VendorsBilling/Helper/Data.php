<?php

class VES_VendorsBilling_Helper_Data extends Mage_Core_Helper_Abstract
{
	const XML_PATH_CREDIT_BALANCE_CHANGE 		= 'vendors/billing/balance_change_template';
	const XML_PATH_CREDIT_NEW_WITHDRAWAL 		= 'vendors/billing/new_withdrawal_template';
    const XML_PATH_CREDIT_WITHDRAWAL_SUCCESS	= 'vendors/billing/withdrawal_success_template';
    const XML_PATH_CREDIT_WITHDRAWAL_REJECTED 	= 'vendors/billing/withdrawal_rejected_template';
    const XML_PATH_CREDIT_EMAIL_IDENTITY		= 'vendors/billing/email_identity';
    
    /**
     * Add currency to price/amount
     * @param int $amount
     * @return string
     */
	public function formatCredit($amount){
		return Mage::helper('core')->currency($amount);
	}

    /**
     * Add currency to price/amount
     * @param int $amount
     * @return string
     */
    public function formatBilling($amount){
        return Mage::helper('core')->currency($amount);
    }
	
	/**
	 * Get transaction description
	 * @param VES_VendorsBilling_Model_Transaction $transaction
	 */
	public function getTransactionDescription(VES_VendorsBilling_Model_Transaction $transaction){
		return Mage::getModel('vendorsbilling/type')->getDescription($transaction);
	}
		
	
	/**
	 * 
	 * Send new withdrawal notification email to vendor
	 * @param VES_VendorsBilling_Model_Withdrawal $withdrawal
	 */
	public function sendNewWithdrawalNotificationEmail(VES_VendorsBilling_Model_Withdrawal $withdrawal){
        $vendor = Mage::getModel('vendors/vendor')->load($withdrawal->getVendorId());
        if(!$vendor->getId()) return;
        $withdrawal->setData('requested_on',Mage::getModel('core/date')->date('M d, Y h:s:i A',$withdrawal->getCreatedAt()));
        $withdrawal->setData('amount',$this->formatCredit($withdrawal->getAmount()));
        $withdrawal->setData('fee',$this->formatCredit($withdrawal->getFee()));
        $withdrawal->setData('net_amount',$this->formatCredit($withdrawal->getNetAmount()));
        $this->_sendEmailTemplate(self::XML_PATH_CREDIT_NEW_WITHDRAWAL, self::XML_PATH_CREDIT_EMAIL_IDENTITY,
            array('vendor' => $vendor, 'withdrawal' => $withdrawal),null,$vendor);
        return $this;
	}
	
	/**
	 * 
	 * Send success withdrawal notification email to vendor
	 * @param VES_VendorsBilling_Model_Withdrawal $withdrawal
	 */
	public function sendSuccessWithdrawalNotificationEmail(VES_VendorsBilling_Model_Withdrawal $withdrawal){
        $vendor = Mage::getModel('vendors/vendor')->load($withdrawal->getVendorId());
        if(!$vendor->getId()) return;
        $withdrawal->setData('requested_on',Mage::getModel('core/date')->date('M d, Y h:s:i A',$withdrawal->getCreatedAt()));
        $withdrawal->setData('amount',$this->formatCredit($withdrawal->getAmount()));
        $withdrawal->setData('fee',$this->formatCredit($withdrawal->getFee()));
        $withdrawal->setData('net_amount',$this->formatCredit($withdrawal->getNetAmount()));
        $this->_sendEmailTemplate(self::XML_PATH_CREDIT_WITHDRAWAL_SUCCESS, self::XML_PATH_CREDIT_EMAIL_IDENTITY,
            array('vendor' => $vendor, 'withdrawal' => $withdrawal),null,$vendor);
        return $this;
	}
	
	/**
	 * 
	 * Send success withdrawal notification email to vendor
	 * @param VES_VendorsBilling_Model_Withdrawal $withdrawal
	 */
	public function sendRejectedWithdrawalNotificationEmail(VES_VendorsBilling_Model_Withdrawal $withdrawal){
        $vendor = Mage::getModel('vendors/vendor')->load($withdrawal->getVendorId());
        if(!$vendor->getId()) return;
        $withdrawal->setData('requested_on',Mage::getModel('core/date')->date('M d, Y h:s:i A',$withdrawal->getCreatedAt()));
        $withdrawal->setData('amount',$this->formatCredit($withdrawal->getAmount()));
        $withdrawal->setData('fee',$this->formatCredit($withdrawal->getFee()));
        $withdrawal->setData('net_amount',$this->formatCredit($withdrawal->getNetAmount()));
        $this->_sendEmailTemplate(self::XML_PATH_CREDIT_WITHDRAWAL_REJECTED, self::XML_PATH_CREDIT_EMAIL_IDENTITY,
            array('vendor' => $vendor, 'withdrawal' => $withdrawal),null,$vendor);
        return $this;
	}
	/**
	 * Send billing balance change notification email to vendor
	 * @param VES_VendorsBilling_Model_Transaction $transaction
	 */
	public function sendCreditBalanceChangeNotificationEmail(VES_VendorsBilling_Model_Transaction $transaction){
		$vendor = Mage::getModel('vendors/vendor')->load($transaction->getVendorId());
        if(!$vendor->getId()) return;
        
        $type = Mage::getModel('vendorsbilling/type')->getType($transaction->getType());
        $sign = $type['action']=='add'?'+':'-';
        
        $transaction->setData('amount',$sign.$this->formatBilling($transaction->getAmount()));
        $transaction->setData('fee','-'.$this->formatBilling($transaction->getFee()));
        $transaction->setData('net_amount',$sign.$this->formatBilling($transaction->getNetAmount()));
        $transaction->setData('balance',$this->formatBilling($transaction->getBalance()));
        $transaction->setData('created_at',Mage::getModel('core/date')->date('M d, Y h:s:i A',$transaction->getCreatedAt()));
        $this->_sendEmailTemplate(self::XML_PATH_CREDIT_BALANCE_CHANGE, self::XML_PATH_CREDIT_EMAIL_IDENTITY,
            array('vendor' => $vendor, 'transaction' => $transaction),null,$vendor);
	}
	
	/**
     * Send corresponding email template
     *
     * @param string $emailTemplate configuration path of email template
     * @param string $emailSender configuration path of email identity
     * @param array $templateParams
     * @param int|null $storeId
     * @return VES_Vendors_Model_Vendor
     */
    protected function _sendEmailTemplate($template, $sender, $templateParams = array(), $storeId = null,$vendor)
    {
        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($vendor->getEmail(), $vendor->getName());
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig($sender, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId(Mage::getStoreConfig($template, $storeId));
        $mailer->setTemplateParams($templateParams);
        $mailer->send();
        return $this;
    }

    public function generateBillingProductSku() {
        $prefix = 'billing_vendor';
        $random = rand(1000,9999);
        $time = strtotime(now());

        return $prefix.'_'.$random.'_'.$time;
    }

    public function getBillingAttributeSet() {
        return Mage::getStoreConfig('vendors/billing/billing_attribute_set');
    }
}