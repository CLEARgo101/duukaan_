<?php

class VES_AdvancedFaq_Model_Observer
{
	public function initControllerRouters($observer) 
    {
    	$request = $observer->getData("request");
    	$condition_ob = $observer->getData("condition");


    	$identifierUrl = $condition_ob->getData('identifier');
    	$identifiers = explode("/", $identifierUrl);
    	if($identifiers[0] != Mage::getStoreConfig('advancedfaq/config/url_key')){
    		return true;
    	}
    	
	    if(sizeof($identifiers) == 1){
			$pageType 	= 'kbase_home';
		}elseif(sizeof($identifiers) == 2){
			if($identifiers[1] == "new"){
				$identifier = $identifiers[1];
				$pageType 	= 'news';
			}else{
				if($identifiers[1] == "search"){
					$identifier = $identifiers[1];
					$pageType 	= 'searchs';
				}else{
					$identifier = $identifiers[1];
					$pageType 	= 'category';
				}
			}
		}else{
			if($identifiers[2]=='voteajax' && $identifiers[1]=='home'){
				$pageType 	= 'votes';
				$id = $identifiers[3];
				$value = $identifiers[4];
			}
			else{
				if($identifiers[2]=='save' && $identifiers[1]=='new'){
					$pageType 	= 'news_save';
				}else{
					$catId 		= $identifiers[1];
					$identifier = $identifiers[2];
					$pageType 	= 'article';
				}
			}
		}
    		 
    		
    	$condition = new Varien_Object(array(
				'identifier' => $identifier,
				'continue'   => true
		));
		Mage::dispatchEvent('advancedfaq_controller_router_match_before', array(
			'router'    => $this,
			'condition' => $condition
		));
		
		if ($condition->getRedirectUrl()) {
			Mage::app()->getFrontController()->getResponse()
			->setRedirect($condition->getRedirectUrl())
			->sendResponse();
			$request->setDispatched(true);
			return true;
		}
		 
		if (!$condition->getContinue()) {
			return false;
		}
		 
		$suffix = '';

		$identifier = $condition->getIdentifier();
		$storeId	= Mage::app()->getStore()->getId();
		
	
		
		if($pageType == 'kbase_home'){

			$request->setModuleName('faq')
			->setControllerName('home')
			->setActionName('index');

			$condition_ob->setDispatched(true);
		
			return true;
		}elseif($pageType=='category'){
			/*Category Page */
			$suffix = Mage::getStoreConfig('advancedfaq/config/category_suffix');
			$tmpIdentifier 	= trim(str_replace($suffix, "", $identifier));
			 
			$category 		= Mage::getModel('advancedfaq/category')->checkIdentifier($tmpIdentifier, $storeId);
			 
			if (!$category->getId()) {
				return false;
			}

			
			
			Mage::register('kbase_category', $category);
			Mage::register('kbase_current_category', $category);
			 
			$request->setModuleName('faq')
			->setControllerName('category')
			->setActionName('index')
			->setParam('identifier', $category->getUrlKey());
			$condition_ob->setDispatched(true);
		}else{
			if($pageType=='votes'){
				$request->setModuleName('faq')
				->setControllerName('home')
				->setActionName('voteajax')
				->setParam('id', $value);
				$condition_ob->setDispatched(true);
			}else{
				if($pageType=='news'){
					$request->setModuleName('faq')
					->setControllerName('new')
					->setActionName('index');
					$condition_ob->setDispatched(true);
				}else{
					if($pageType=='searchs'){
						$request->setModuleName('faq')
						->setControllerName('search')
						->setActionName('index');
						$condition_ob->setDispatched(true);
					}else{
						if($pageType=='news_save'){
							$request->setModuleName('faq')
							->setControllerName('new')
							->setActionName('save');
							$condition_ob->setDispatched(true);
						}
					}
				}
			}
		}
    		
    	$request->setAlias(
    		Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
    		$identifierUrl
    	);
    	return true;
    }
    
}