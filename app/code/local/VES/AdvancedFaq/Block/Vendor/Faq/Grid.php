<?php

class VES_AdvancedFaq_Block_Vendor_Faq_Grid extends VES_AdvancedFaq_Block_Adminhtml_Faq_Grid
{

  protected function _prepareCollection()
  {
  	  $tbl_faq_item = Mage::getSingleton('core/resource')->getTableName('advancedfaq/category');
  	  $collection = Mage::getModel('advancedfaq/faq')->getCollection();
  	  $vendorId = Mage::getSingleton('vendors/session')->getVendorId();
  	  $collection->addFieldToFilter('vendor_id',$vendorId);
      $this->setCollection($collection);
      return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
  }

}