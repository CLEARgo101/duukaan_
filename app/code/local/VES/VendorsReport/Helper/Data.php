<?php
class VES_VendorsReport_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function moduleEnable(){
		$result = new Varien_Object(array('module_enable'=>true));
		Mage::dispatchEvent('ves_vendorsreport_module_enable',array('result'=>$result));
		return $result->getData('module_enable');
	}
}