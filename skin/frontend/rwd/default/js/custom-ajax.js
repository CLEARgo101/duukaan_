$j(document).ready(function () {
	$j.fn.getLastestDeals = function () {
		var _GET_URL = BASE_URL;
		_GET_URL += 'custom-block/ajax/get-product/categories/56';
		var _this = $j(this);
		$j.ajax({
			type: "GET",
			cache: false,
			url: _GET_URL,
			success: function (response) {
				_this.html(response);

				// calculate width for each slide
				var pageWidth = $j('.page').width(),
						slideMargin = 20,
						slideWidth = pageWidth / 6 - slideMargin * 5 / 6,
						slideCount = _this.find('li').length,
						option = {
							maxSlides: slideCount > 6 ? 6 : slideCount,
							minSlides: slideCount > 6 ? 6 : slideCount,
							slideWidth: slideWidth,
							slideMargin: slideMargin,
							nextText: '',
							prevText: '',
							pager: false,
							infiniteLoop: false
						};
				//console.log(slideCount);

				if (detect_mobile()) {
					option.maxSlides = 1;
					option.minSlides = 1;
					option.slideWidth = pageWidth;
				}

				var mySlider = _this.find('ul').bxSlider(option);
				$j('.homepage-lastest-deals-content .bx-viewport .products').getAppendLastestDeals(mySlider);
			}
		});
	};

	$j.fn.getAppendLastestDeals = function (mySlider) {
		var _GET_URL = BASE_URL;
		_GET_URL += 'custom-block/ajax/get-product/categories/56';
		var _this = $j(this);
		$j.ajax({
			type: "GET",
			cache: false,
			data :{
				type: 'append'
			},
			url: _GET_URL,
			success: function (response) {
				_this.append(response);

				var currentSlide = mySlider.getCurrentSlide();
				var pageWidth = $j('.page').width(),
					slideMargin = 20,
					slideWidth = pageWidth / 6 - slideMargin * 5 / 6,
					slideCount = $j('.homepage-lastest-deals-content').find('li').length,
					option = {
						startSlide: currentSlide,
						maxSlides: slideCount > 6 ? 6 : slideCount,
						minSlides: slideCount > 6 ? 6 : slideCount,
						slideWidth: slideWidth,
						slideMargin: slideMargin,
						nextText: '',
						prevText: '',
						pager: false,
						infiniteLoop: false
					};
				//console.log(slideCount);

				if (detect_mobile()) {
					option.maxSlides = 1;
					option.minSlides = 1;
					option.slideWidth = pageWidth;
				}

				mySlider.reloadSlider(option);
			}
		});
	};

	if ($j('.homepage-lastest-deals-content').length > 0) {
		$j('.homepage-lastest-deals-content').getLastestDeals();
	}

	$j.fn.getFeaturedCategoriesProducts = function ($categories) {
		var _GET_URL = BASE_URL;
		_GET_URL += 'custom-block/ajax/featured-category/categories/' + $categories + '/page-size/4';
		var _this = $j(this);
		_this.html('<img src="' + SKIN_URL + '/images/ajax-loader.gif" class="loading" />');
		$j.ajax({
			type: "GET",
			cache: false,
			url: _GET_URL,
			success: function (response) {
				_this.html(response);

				if (detect_mobile()) {
					// Stup bxSlider for mobile version
					var $thumbs = $j('.categories-featured').find('.products'),
							slideWidth = $thumbs.width() / 2;

					_this.find('.products').bxSlider({
						maxSlides: 2,
						minSlides: 2,
						slideWidth: slideWidth,
						slideMargin: 0,
						nextText: '',
						prevText: '',
						pager: false,
						infiniteLoop: false
					});
				} else {
					// At sub-category, show scrollbar if there've more than 6 items
					var $subList = _this.closest('.categories-featured').find('.categories-featured-selector > ul'),
							$productWrapper = _this.find('.categories-featured-product-wrapper'),
							baseHeight = $productWrapper.height();
					if (baseHeight < 310) baseHeight = 310;

					$subList.css('height', baseHeight);
					$productWrapper.css('height', baseHeight);
					$productWrapper.find('.category-thumbnail').css('height', baseHeight);
					$subList.mCustomScrollbar({
						scrollInertia: 500
					});
				}

				$j('.featured-category-tab').click(function () {
					$j(this).clickFeaturedCategoryTab();
				});
			}
		});
	};

	$j.fn.clickFeaturedCategoryTab = function () {
		$j(this).siblings().removeClass('active');
		$j(this).addClass('active');
		var parent = $j(this).parents('.categories-featured');
		var category_id = '';
		if (parent.find('.featured-category-sub.active').data('id') != null)
			category_id = parent.find('.featured-category-sub.active').data('id');

		if (parent.find('.featured-category-tab.active').data('id') != null) {
			if (category_id == '') {
				category_id = parent.find('.featured-category-tab.active').data('id');
			} else {
				category_id += ',' + parent.find('.featured-category-tab.active').data('id');
			}
		}
		parent.find('.ajax-result').getFeaturedCategoriesProducts(category_id);

	}
	$j('.featured-category-sub').click(function () {
		$j(this).clickFeaturedCategoryTab();
	});
	//console.log($j('.categories-featured .featured-category-sub.active'));
	$j('.categories-featured .featured-category-sub.active').each(function () {
		$j(this).clickFeaturedCategoryTab();
	});
///    $j('.categories-featured .featured-category-sub.active').clickFeaturedCategoryTab();
});///test
