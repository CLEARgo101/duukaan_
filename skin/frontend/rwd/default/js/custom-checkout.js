var CustomCheckout = Class.create();

Billing.prototype.saveCustomBilling = function() {
    //if (checkout.loadWaiting!=false) return;
    //console.log(this.form);
    var validator = new Validation(this.form);
    //console.log(validator);
    //validator.initialize(this.form, {});
    //var customForm = new VarienForm('co-billing-form');
    //console.log(customForm);
    //validator.options.useTitles = false;
    if (validator.validate()) {
        //checkout.setLoadWaiting('billing');

//            if ($('billing:use_for_shipping') && $('billing:use_for_shipping').checked) {
//                $('billing:use_for_shipping').value=1;
//            }
       // console.log(Form.serialize(this.form));
        //console.log(Form.serialize(shipping.form));

        //return;
        var request = new Ajax.Request(
            BASE_URL+'checkout/onepage/saveCustomBilling',
            {
                method: 'post',
                onComplete: this.saveCustomBillingComplete,
                onSuccess: this.saveCustomBillingSuccess,
                onFailure: checkout.ajaxFailure.bind(checkout),
                parameters: Form.serialize(this.form)+'&'+Form.serialize(shipping.form)
            }
        );
    }
}
Billing.prototype.saveCustomBillingSuccess = function() {

}
Billing.prototype.saveCustomBillingComplete = function($response) {
    //console.log($response);
    var request = new Ajax.Request(
        BASE_URL+'checkout/onepage/shippingMethod',
        {
            method: 'post',/*
            onComplete: this.saveCustomBillingComplete,
            onSuccess: this.saveCustomBillingSuccess,
            onFailure: checkout.ajaxFailure.bind(checkout),
            parameters: Form.serialize(this.form)+'&'+Form.serialize(shipping.form)*/
        }
    );
}
CustomCheckout.setBillingShipAddress = function() {
    $j('input[name="billing[use_for_shipping]"]').change(function(){
        if($j(this).attr('id') == 'billing:use_for_shipping_no') {
            if($j(this).is(':checked') == true) {
                $j('.shipping-infomation').show();
            }
        }else {
            $j('.shipping-infomation').hide();
        }
    });
}
CustomCheckout.saveCustomBilling = function() {
    billing.saveCustomBilling();
}
CustomCheckout.placeCustomOrder = function () {
    //console.log(Billing.form);
    billing.saveCustomBilling();
}


$j(document).ready(function () {
    CustomCheckout.setBillingShipAddress();

    $j('#checkout-playorder').click(function(){
        //CustomCheckout.placeCustomOrder();
    });
    $j('.continue-checkout').click(function(){
        CustomCheckout.saveCustomBilling();
    });
});
