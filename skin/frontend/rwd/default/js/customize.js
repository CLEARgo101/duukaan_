$j(document).ready(function () {
	$j.fn.selectbox_initialize = function () {
		if($j('select#select-language').length > 0) {
			$j('select#select-language').selectbox({});
		}
		//console.log($j('select.category-search-select').length);
		if($j('select.category-search-select').length > 0) {
			$j('select.category-search-select').selectbox({});
		}
		//$j('select.category-search-select').selectbox({});
	}
	$j.fn.product_detail_qty_initialize = function () {
		$j('a.decrease-qty').click(function () {
			var val = parseInt($j(this).parents('.qty-field').find('#qty').val());
			if (val > 1) {
				val -= 1;
			}
			$j(this).parents('.qty-field').find('#qty').val(val);
		});
		$j('a.increase-qty').click(function () {
			var val = parseInt($j(this).parents('.qty-field').find('#qty').val());
			val += 1;
			$j(this).parents('.qty-field').find('#qty').val(val);
		});
	};

	var allPanels = $j('#narrow-by-list > dd').hide();

	$j.fn.product_detail_tabs_initialize = function () {
		var _tab_hash = window.location.hash;
		if (_tab_hash != '') {
			$j('.product-collateral .nav-tabs li').removeClass('active');
			$j('.product-collateral .nav-tabs li a[href="' + _tab_hash + '"]').parent().addClass('active');
			$j('.product-collateral .tab-content .tab-pane').hide();
			$j('.product-collateral .tab-content ' + _tab_hash).show();
		}
		$j('.product-collateral .nav-tabs').find('li a').click(function () {
			$j(this).parent().siblings().removeClass('active');
			$j(this).parent().addClass('active');
			var tab_id = $j(this).attr('href');
			$j('.product-collateral .tab-content .tab-pane').hide();
			$j('.product-collateral .tab-content ' + tab_id).show();
		});

		$j('#reviews .pages a').each(function () {
			var _href = $j(this).attr('href');
			$j(this).attr('href', _href + '#reviews');
		});

	};

	$j('.go-to-short-description').click(function(e){
		
		$j(".nav-tabs-description").trigger("click", function(){
			
		});
		$j('html, body').animate({
            scrollTop: $j("#description").offset().top
        }, 1000);
        e.preventDefault();
	});
	$j(".sticky-newsletter-title").click(function () {
		$j(this).parents('.sticky-newsletter').addClass('active');
	});

	$j(".sticky-newsletter-close").click(function () {
		$j(this).parents('.sticky-newsletter').removeClass('active');
	});


	$j.fn.selectbox_initialize();
	if($j('.product-collateral').length > 0) {
		$j.fn.product_detail_qty_initialize();
		$j.fn.product_detail_tabs_initialize();
	}

	if (detect_mobile() == true) {
		$j("#main-navigation .menu-item.categories").click(function () {
			console.log('aa');
			if ($j("#nav").hasClass("active")){
				$j("#nav").removeClass('active').slideUp();
			}
			else {
				$j('#nav').addClass('active').slideDown();
			}
		});
	}


	if (detect_mobile() == true) {
		$j(".nav-primary .parent .mb-show-child").click(function () {
			// var $childWrapper = $j(this).siblings(".categories-menu-child-wrapper");
			var $navigation = $j('#main-navigation'),
				activeIndex = $navigation.find(".mb-show-child.active").parent().index();
			$navigation.find(".categories-menu-child-wrapper.active").removeClass('active');
			$navigation.find(".mb-show-child.active").removeClass('active');

			if($j(this).parent().index() != activeIndex) {
				$j(this).addClass('active');
				$j(this).siblings('.categories-menu-child-wrapper').addClass('active');
			}
			// if (target.hasClass("active")){
			// 	target.removeClass('active');
			// 	$j(this).removeClass('active');
			// }
			// else {
			// 	$j(".categories-menu-child-wrapper").removeClass('active');
			// 	$j(".mb-show-child").removeClass('active');
			// 	target.addClass('active');
			// 	$j(this).addClass('active');
			// }
		});
	}

	// $j('.catalog-product-view .who-bought-also-bought').addClass('Slider');
	if($j('.Slider').length > 0) {
		$j('.Slider').bxSlider({
			slideWidth: 275,
			minSlides: 1,
			maxSlides: 6,
			moveSlides: 1,
			slideMargin: 15,
			pager: false,
			infiniteLoop: false,
			auto: ($j('.Slider').children().length < 6) ? false : true,
		});
	}

	$j('.logged-in').hover(function () {
		var target = $j('.logged-in ul');
		if (target.hasClass('active')) {
			target.removeClass('active');
			$j('.logged-in').removeClass('active');
		} else {
			target.addClass('active');
			$j('.logged-in').addClass('active');
		}
	});

	$j('.form-language .sbHolder a').click(function () {
		var target = $j(this).parents('.store-language-container');
		  
		if (target.hasClass('active')) {
			target.removeClass('active');
			 
		} else {
			target.addClass('active');
		 

		}
	});

	$j('.Faq-content').hide();

	$j('.Faq-title').click(function () {
		function scrollpage() {
			var offset = $j(this).prev().offset().top;
		 	console.log(offset);
			$j("html, body").animate({
	            scrollTop: offset
	        }, 600);
		}

		var target = $j(this).next();
		
		if (target.hasClass('active')) {
	        return false;
			target.removeClass('active').slideUp(scrollpage);
			$j(this).removeClass('active');
		}
		else {
			$j('.Faq-content').removeClass('active').slideUp();
			$j('.Faq-title').removeClass('active');
			target.addClass('active').slideDown(scrollpage);
			
			$j(this).addClass('active');

		}
	});

	
	// $j(document).hover(function(e){
	// 	var parent = $j(e.target).closest('.li-payon-delivery');
	// 	if(!parent.length) {
	// 		$j('.popup-pay-mobile-menu-dis').removeClass('active').slideUp();
	// 	}
	// });

	// $j(document).hover(function(e){
	// 	var parent = $j(e.target).closest('.li-email-customer');
	// 	if(!parent.length) {
	// 		$j('.popup-email-menu-dis').removeClass('active').slideUp();
	// 	}
	// });

	// $j(document).hover(function(e){
	// 	var parent = $j(e.target).closest('.li-phone-app-support');
	// 	if(!parent.length) {
	// 		$j('.popup-phone-support-menu-dis').removeClass('active').slideUp();
	// 	}
	// });
	// if(detect_mobile()==true){
	// 	$j(document).click(function(e){
	// 		var parent_menu= $j(e.target).closest('#main-navigation > li:first-child');
	// 		if (!parent_menu.length){
	// 			$j('#nav').removeClass('active').slideUp();
	// 		}
	// 	});
	// }

	// // $j('.bx-controls .bx-controls-direction a').removeClass('disabled');
	// $j("#main-navigation .li-payon-delivery").hover(function(){
	// 	var target = $j(".popup-pay-mobile-menu-dis");
	// 	if (target.hasClass('active')) {
	// 		target.removeClass('active').slideUp();
	// 		$j(this).removeClass('active');
	// 	}
	// 	else {
	// 		$j('.popup-pay-mobile-menu-dis').removeClass('active').slideUp();
	// 		target.addClass('active').slideDown();
	// 		$j(this).addClass('active');
	// 	}
	// });

	// $j("#main-navigation .li-email-customer").hover(function(){
	// 	var target = $j(".popup-email-menu-dis");
	// 	if (target.hasClass('active')) {
	// 		target.removeClass('active').slideUp();
	// 		$j(this).removeClass('active');
	// 	}
	// 	else {
	// 		$j('.popup-email-menu-dis').removeClass('active').slideUp();
	// 		target.addClass('active').slideDown();
	// 		$j(this).addClass('active');
	// 	}
	// });

	// $j("#main-navigation .li-phone-app-support").hover(function(){
	// 	var target = $j(".popup-phone-support-menu-dis");
	// 	if (target.hasClass('active')) {
	// 		target.removeClass('active').slideUp();
	// 		$j(this).removeClass('active');
	// 	}
	// 	else {
	// 		$j('.popup-phone-support-menu-dis').removeClass('active').slideUp();
	// 		target.addClass('active').slideDown();
	// 		$j(this).addClass('active');
	// 	}
	// });


});



(function (window, document, $) {

	$(document).ready(function () {
		setTimeout(function () {
			calculateSubNavSize();
		}, 500);
		setupBxSliderForProductDetails();
		setupBxSliderForSameBuying();
		setupBxSliderForRelatedProduct();
	});

	$(window).resize(function () {
		calculateSubNavSize();
	});

	function calculateSubNavSize() {
		var navWidth = $('#main-navigation').width() - $('#nav').width();
		var navheight = $('.slides').height() + 1;
		if($(window).width() > 800){
			$('.categories-menu-child-wrapper')
					.css('width', navWidth)
					.css('height', navheight + 1);
			$('.cms-index-index #nav')
					.css('height', navheight);
		}
	}

	/*
	 * method: Setup bxSlider for product details thumbnails
	 * */
	function setupBxSliderForProductDetails() {
		setupBxSlider('.product-view .product-image-thumbs', 4, 4);
		// var $thumbs = $('.product-view').find('.product-image-thumbs'),
		// 		slideMargin = detect_mobile() ? 5 : 15,
		// 		slideWidth = ($thumbs.width() - 30) / 4 - slideMargin;
		//
		// if ($thumbs.length) {
		// 	$thumbs.bxSlider({
		// 		minSlides: 4,
		// 		maxSlides: 4,
		// 		slideWidth: slideWidth,
		// 		slideMargin: slideMargin,
		// 		nextText: '',
		// 		prevText: '',
		// 		pager: false,
		// 		infiniteLoop: false
		// 	});
		// }
	}

	/*
	 * method: Setup bxSlider for same buying
	 * */
	function setupBxSliderForSameBuying() {
		setupBxSlider('.who-bought-also-bought', 2, 6);
	}

	/*
	 * method: Setup bxSlider for related product
	 * */
	function setupBxSliderForRelatedProduct() {
		setupBxSlider('#custom-recently-viewed-items', 2, 6);
	}

	function setupBxSlider(selector, minSlide, maxSlide) {
		var $thumbs = $(selector),
				pageWidth = $thumbs.width(),
				slideCount = detect_mobile() ? minSlide : maxSlide,
				slideMargin = detect_mobile() ? 5 : 15,
				slideWidth = (pageWidth - slideMargin * (slideCount - 1)) / slideCount;

		if ($thumbs.length) {
			$thumbs.bxSlider({
				maxSlides: slideCount,
				minSlides: slideCount,
				slideWidth: slideWidth,
				slideMargin: slideMargin,
				nextText: '',
				prevText: '',
				pager: false,
				infiniteLoop: false
			});
		}
	}

	var allPanels = $('.faq-content .kb-articles-listing .faq-title').hide();

	$(document).on("click", ".faq-title", function () {
		$this = $(this);
		$target = $this.next();
		if ($target.hasClass('active')) {

			$target.removeClass('active').slideUp();
			$this.removeClass('active');

		}
		else {
			allPanels.removeClass('active').slideUp();
			$target.addClass('active').slideDown();
			$this.addClass('active');
		}
		return false;
	});

	$(document).on('click', '.show-more-categories-featured', function () {
		var $me = $(this),
				$target = $me.prev();
		console.log($target);
		$target.slideToggle(200);
	});
	// console.log('detect_mobile() : ' + detect_mobile());

}(window, document, $j));

function detect_mobile() {
	var mobile = false;
	if (navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPhone/i)) {
		//Page.callMobile();
		mobile = true;
//    console.log(1);
	}

	if (navigator.userAgent.match(/iPad/i)) {
		//Page.callDesktop();
		mobile = true;
	}

	if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/BlackBerry/i)) {
		/*
		 if(window.innerWidth <= 640){
		 Page.callMobile();
		 }else{
		 Page.callDesktop();
		 } */
		mobile = true;
	}

	if (navigator.userAgent.match(/Windows Phone/i)) {
		/*if(window.innerWidth <= 1024){
		 Page.callMobile();
		 }else{
		 Page.callDesktop();
		 } */
		mobile = true;
	}
	if(window.innerWidth < 1024){
		mobile = true;
	}
	// console.log(navigator.userAgent);
//  console.log(mobile);
	return mobile
}
