@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION
FOR /R %%i IN (*) DO (
    SET "n=%%~nxi"
    REN "%%i" "!n: =_!"
)