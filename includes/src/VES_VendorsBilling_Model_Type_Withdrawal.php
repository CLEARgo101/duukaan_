<?php
class VES_VendorsBilling_Model_Type_Withdrawal extends VES_VendorsBilling_Model_Type_Abstract
{
	public function process(){
		$vendor 		= $this->getVendor();
    	$type			= $this->getType();
    	$amount			= $this->getAmount();
    	$fee			= 0;
    	$netAmount		= $amount;
    	$withdrawal		= $this->getWithdrawal();
    	$action 		= $this->getAction();
		
    	/*Do nothing if the amount is zero*/
    	if(!$amount) return;
    	
    	$this->processAmount($vendor, $action, $netAmount);
    	
    	$description	= Mage::helper('vendorsbilling')->__('Withdraw money using "%s"',$withdrawal->getMethod());
    	//$additionalInfo	= serialize(array('withdrawal_id'=>$withdrawal->getId(),'note'=>$withdrawal->getNote()));
    	$additionalInfo	= 'withdrawal_id|'.$withdrawal->getId().'||note|'.$withdrawal->getNote();
    	
    	/*Save transaction*/
    	$transaction = Mage::getModel('vendorsbilling/transaction')->setData(array(
    		'vendor_id'			=> $vendor->getId(),
    		'type'				=> $type,
    		'amount'			=> $amount,
    		'fee'				=> $fee,
	    	'net_amount'		=> $netAmount,
	    	'balance'			=> $vendor->getCredit(),
	    	'description'		=> $description,
	    	'additional_info'	=> $additionalInfo,
	    	'created_at'		=> now(),
    	))->save();
	}
	
	public function getDescription(VES_VendorsBilling_Model_Transaction $transaction){
		$additionalInfo = explode('||',$transaction->getAdditionalInfo());
		$addInfo = array();
		foreach($additionalInfo as $data){
			$tmpData = explode('|',$data);
			if(sizeof($tmpData) == 2){
				$addInfo[$tmpData[0]] = $tmpData[1];
			}
		}
		$withdrawalId	= $addInfo['withdrawal_id'];
		$withdrawal 	= Mage::getModel('vendorsbilling/withdrawal')->load($withdrawalId);
		if($withdrawal->getId()){
			$description	= Mage::helper('vendorsbilling')->__('Withdraw money using "%s", "%s"',$withdrawal->getMethod(),$addInfo['note']);
		}else{
			$description	= $transaction->getDescription();
		}
		return $description;
	}
}