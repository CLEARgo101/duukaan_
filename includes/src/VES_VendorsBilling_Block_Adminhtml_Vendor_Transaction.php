<?php
class VES_VendorsBilling_Block_Adminhtml_Vendor_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_vendor_transaction';
		$this->_blockGroup = 'vendorsbilling';
		$this->_headerText = Mage::helper('vendorsbilling')->__('Transaction Manager');
		$this->_addButtonLabel = Mage::helper('vendorsbilling')->__('New Transaction');
		parent::__construct();
	}
}