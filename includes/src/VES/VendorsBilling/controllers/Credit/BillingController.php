<?php
class VES_VendorsBilling_Credit_BillingController extends VES_Vendors_Controller_Action
{

    public function indexAction(){
        $vendor = Mage::getSingleton('vendors/session')->getVendor();
        $model = Mage::getModel('vendors/vendor')->load($vendor->getId());
        if(!Mage::registry('vendors_data'))
            Mage::register('vendors_data', $model);
        $this->loadLayout()
            ->_setActiveMenu('billing')->_title($this->__('Billing'))->_title($this->__('Withdraw'))
            ->_addBreadcrumb(Mage::helper('vendorsbilling')->__('Billing'), Mage::helper('vendorsbilling')->__('Billing'));
        $this->renderLayout();

/*        var_dump(Mage::getModel('catalog/product')->load('203')->getData());*/
    }

    public function billingAction() {
        if(!Mage::getSingleton('vendors/session')->getVendor()) return;
        
        $vendor = Mage::getSingleton('vendors/session')->getVendor();
		if(!$vendor->getVesBillingVendor()){
			$this->_getSession()->addError($this->__('Your billing account balance is zero'));
            $this->_redirect('*/*/index');
            return;
		}
        /*check exist quote item in cart*/
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $cart = Mage::getSingleton('checkout/cart');
        $cart->init();

        foreach($cart->getItems() as $item) {
            $productId = $item->getProduct()->getId();
            $product = Mage::getModel('catalog/product')->load($productId);

            /*
             * suck item->getProduct() not return true product model.Loss data.
             */
            if($product->getIsBillingVendor() == '1' && $product->isVirtual())
                $cart->removeItem($item->getItemId());
        }

        /*add new virtual product to cart*/

        $model = Mage::getModel('catalog/product');
        $model->setSku(Mage::helper('vendorsbilling')->generateBillingProductSku())
            ->setTypeId('virtual')
            ->setData('is_billing_vendor','1')//yes
            ->setData('related_vendor_id', $vendor->getId())
            ->setPrice($vendor->getVesBillingVendor())
            ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
            ->setWebsiteIDs(array(1))
            ->setName($this->__('Billing Product - %s - %s',$vendor->getVendorId(),now(true)))
            ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->setTaxClassId(0)
            ->setApproval(2)
            ->setCreatedAt(strtotime('now'))
            ->setAttributeSetId(Mage::helper('vendorsbilling')->getBillingAttributeSet())//???
            ->setDescription($this->__('Billing Product - %s - %s',$vendor->getVendorId(),now(true)))
            ->setShortDescription($this->__('Billing Product - %s - %s',$vendor->getVendorId(),now(true)))
            ->setStockData(array('is_in_stock' => '1','qty' => '1'));

        $model->save();

        $product = Mage::getModel('catalog/product')->load($model->getId());


        try {
            //add to cart
            $cart->addProduct($product,array('product'=>$product->getId(), 'qty' => '1'));
            $cart->save();
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
            $this->_redirect('checkout/cart/index');
        } catch(VES_Vendors_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $this->_redirect('*/*/index');
        }
    }
}