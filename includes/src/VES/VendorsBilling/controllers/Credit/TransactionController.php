<?php
class VES_VendorsBilling_Credit_TransactionController extends VES_Vendors_Controller_Action
{
	public function indexAction(){
    	$this->loadLayout()
		->_setActiveMenu('sales')->_title($this->__('Credit Transactions'))
		->_addBreadcrumb(Mage::helper('vendorssales')->__('Sales'), Mage::helper('vendorssales')->__('Sales'))
    	->_addBreadcrumb(Mage::helper('vendorsbilling')->__('Credit Transactions'), Mage::helper('vendorsbilling')->__('Credit Transactions'));
		$this->renderLayout();
	}
}