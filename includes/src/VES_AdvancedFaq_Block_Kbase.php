<?php
class VES_AdvancedFaq_Block_Kbase extends VES_AdvancedFaq_Block_Abstract
{
	public function _prepareLayout()
    {
    	if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
    		if(Mage::registry("current_vendor")){
    			$vendor = Mage::registry("current_vendor");
    			$url = Mage::helper("vendorspage")->getUrl($vendor);
    			//$url= $this->getUrl($vendor->getData("vendor_id")."/".$this->getUrlKey()."/new");
    		}
    		else{
    			$url = Mage::getUrl();
    		}
            $breadcrumbsBlock->addCrumb('home', array(
                'label'	=> Mage::helper('advancedfaq')->__('Home'),
                'title'	=> Mage::helper('advancedfaq')->__('Home'),
                'link'	=> $url,
            ))->addCrumb('kbasehome', array(
                'label'	=> $this->getHomeTitle(),
                'title'	=> $this->getHomeTitle(),
            ));
    	}
    	
    	if ($headBlock = $this->getLayout()->getBlock('head')) {
			$title = $this->getHomeTitle();
			if($storeName = Mage::getStoreConfig('general/store_information/name')) $title .=' - '.$storeName;
			$headBlock->setTitle($title);
			if($keywords = Mage::helper("advancedfaq")->getMetaKeyword()){
				$headBlock->setKeywords($keywords);
			}
			if($metaDescription = Mage::helper("advancedfaq")->getMetaDescription()){
				$headBlock->setDescription($metaDescription);
			}
		}
		
	
		return parent::_prepareLayout();
    }

    /**
     * Get all categories
     * @return VES_AdvancedFaq_Model_Mysql4_Category_Collection
     */
    public function getAllCategories(){
    	$storeId = Mage::app()->getStore()->getId();
    	$data = array();

    	$con = array(
    		array('finset'=>$storeId),
    		array('finset'=>0),
    	);

    	$category = Mage::getModel("advancedfaq/category")->getCollection()->addFieldToFilter('store_id',$con);

    	if(Mage::registry("current_vendor")){
    		$vendor =  Mage::registry("current_vendor");
    		$category->addFieldToFilter('vendor_id',$vendor->getId());
    	}
        //echo sizeof($category);exit;
    	return $category;
    }

   
    /**
     * Get all faqs related to category
     * @param int $category_id
     * @return VES_AdvancedFaq_Model_Mysql4_Faq_Collection
     */
    public function canShowRatingForm($id){
    	$cookie = Mage::getSingleton('core/cookie');
    	$ratingIds = explode(',',$cookie->get('ves_advancedfaq_rating_ids'));
    	return (!in_array($id, $ratingIds));
    }
    
    public function getFaq($category_id){

    	$faq = Mage::getModel("advancedfaq/faq")->getCollection()->addFieldToFilter('category_id',array('finset'=>$category_id))->addFieldToFilter('show_on',array("eq"=>VES_AdvancedFaq_Model_Status::STATUS_ENABLED))->setOrder('sort_order','ASC');
    	

    	if(Mage::registry("current_vendor")){
    		$vendor =  Mage::registry("current_vendor");
    		$faq->addFieldToFilter('vendor_id',$vendor->getId());
    	}
    	
    	return $faq;
    }

 
    /**
     * sort block
     */
    public function sortBlockHtml(){
    	return $this->getChildHtml("block_faq");
    }

    /**
     * get Url new
     */
    public function getUrlNew(){
    	//return "test";
    	$url= $this->getUrl($this->getUrlKey()."/new");
    	if(Mage::registry("current_vendor")){
    		$vendor = Mage::registry("current_vendor");
    		$url = Mage::helper("vendorspage")->getUrl($vendor,$this->getUrlKey()."/new");
    		//$url= $this->getUrl($vendor->getData("vendor_id")."/".$this->getUrlKey()."/new");
    	}
    	return $url;
    }
}