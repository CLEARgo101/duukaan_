<?php

class KA_CheckLogin_LoginController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$parameter = $this->getRequest()->getParams();
		$vendor = Mage::getModel('vendors/vendor')
				->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
		$results = $vendor->authenticate_customize($parameter['username'],$parameter['password']);
		echo json_encode(array("results" => $results));
	}

	public function getPackageAction()
	{
		$parameter = $this->getRequest()->getParams();
		$_url = str_replace('duukaan_package','checkout/cart/add', $parameter['url']);
		$session = Mage::getSingleton("core/session");
		$session->setData("packageSelect", $_url);
	}
}