<?php
require_once 'Mage/Sales/controllers/OrderController.php';

class KA_CustomSales_OrderController extends Mage_Sales_OrderController
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Sales'));
        $this->renderLayout();
    }
}
