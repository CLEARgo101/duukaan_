<?php

require_once 'Mage/Checkout/controllers/OnepageController.php';

class KA_CustomCheckout_Checkout_OnepageController extends Mage_Checkout_OnepageController
{


    public function indexAction()
    {

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));
        $this->renderLayout();
    }
    public function saveCustomBillingAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('billing', array());
            //$dataShipping = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            //var_dump($dataShipping); die();
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);
            //var_dump($result); die();
            if (!isset($result['error'])) {
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    //$result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    //$result['goto_section'] = 'shipping_method';
                    $result['update_section'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );

                    //$result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
//                    $result['goto_section'] = 'shipping';
                    $dataShipping = $this->getRequest()->getPost('shipping', array());
                    $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
                    $result = $this->getOnepage()->saveShipping($dataShipping, $customerAddressId);

                    if (!isset($result['error'])) {
                        //$result['goto_section'] = 'shipping_method';
                        /*$result['update_section'] = array(
                            'name' => 'shipping-method',
                            'html' => $this->_getShippingMethodsHtml()
                        );*/
                    }
                }
                /*$result['update_section'] = array(
                    'name' => 'payment-method',
                    'html' => $this->_getCustomShippingMethodsHtml()
                );*/
                $result['goto_step'] = 'step-payment';
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
    public function customShippingmethodAction()
    {

        $this->loadLayout();
        $this->renderLayout();
    }
}