<?php
class VES_AdvancedFaq_Block_Search_Result extends VES_AdvancedFaq_Block_Abstract
{
	public function __construct(){
		$this->setTemplate("ves_advancedfaq/search/theme1/result.phtml");
		return parent::__construct();
	}
	public function _prepareLayout()
	{
		parent::_prepareLayout();
	
	
		if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {

			$link = array(
					'label'	=> $this->getHomeTitle(),
					'title'	=> $this->getHomeTitle(),
					'link'	=> Mage::helper('advancedfaq')->getUrlBase().$this->getUrlKey(),
					//'link'	=> Mage::helper('advancedfaq')->getUrlBase()."faq/home",
			);
				
			if(Mage::registry("current_vendor")){
    			$vendor = Mage::registry("current_vendor");
    			$url = Mage::helper("vendorspage")->getUrl($vendor);
    			$link['link'] = Mage::helper("vendorspage")->getUrl($vendor,$this->getUrlKey());
    		}
    		else{
    			$url = Mage::getUrl();
    		}
			
			$breadcrumbsBlock->addCrumb('home', array(
					'label'	=> Mage::helper('advancedfaq')->__('Home'),
					'title'	=> Mage::helper('advancedfaq')->__('Home'),
					'link'	=> $url,
			))->addCrumb('kbasehome',$link)
			->addCrumb('search', array(
					'label'	=> Mage::helper('advancedfaq')->__('Search results'),
					'title'	=> Mage::helper('advancedfaq')->__('Search results'),
			));
		}
		if ($headBlock = $this->getLayout()->getBlock('head')) {
			if(!$title) $title = "Search Results".' - '.$this->getHomeTitle();
			$headBlock->setTitle($title);
			if($keywords = Mage::helper("advancedfaq")->getMetaKeyword()){
				$headBlock->setKeywords($keywords);
			}
			if($metaDescription = Mage::helper("advancedfaq")->getMetaDescription()){
				$headBlock->setDescription($metaDescription);
			}
		}
		return $this;
	}
	
	/**
	 *
	 * get Description Chart
	 */
	public function getDescriptionChart(){
		return Mage::helper('advancedfaq')->getDescriptionLength() ;
	}
	
    public function getKeyQuery(){
    	$key = "";
    	if(Mage::registry("key_query")) $key = Mage::registry("key_query");
 
    	return $key;
    }
    
  
    
    public function getFaqs(){
    	$key = "";
    	if(Mage::registry("key_query")) $key = Mage::registry("key_query");
    	//$faq = Mage::getModel('kbase/faq')->getCollection()->addFieldToFilter("question",array('like' =>'%'.$key.'%'));
    	$faqs = Mage::getModel('advancedfaq/faq')->getCollection()->addFieldToFilter("show_on",array('eq' =>VES_AdvancedFaq_Model_Status::STATUS_ENABLED));
    	
    	if(Mage::registry("current_vendor")){
    		$vendor =  Mage::registry("current_vendor");
    		$faqs->addFieldToFilter('vendor_id',$vendor->getId());
    	}
    	//var_dump($faqs);exit;
    	$data = array();
    	$faq_after = array();
    	foreach ($faqs as $faq){
    		$cate = Mage::getModel("advancedfaq/category")->load($faq->getData('category_id'));
            $store_ids = explode(",",$cate->getData("store_id"));
            $store = Mage::app()->getStore()->getId();

    		if(!in_array($store,$store_ids)) continue;
    		if(preg_match("/".$key."/is",$faq->getData('question'),$result)){
    			$data[] = $faq;
    		}
    		else{
    			$faq_after[] = $faq;
    		}
    	}
    	foreach ($faq_after as $faq){
    		if(preg_match("/".$key."/is",$faq->getData('answer'),$result)){
    			$data[] = $faq;
    		}
    	}
    	return $data;
    }
}