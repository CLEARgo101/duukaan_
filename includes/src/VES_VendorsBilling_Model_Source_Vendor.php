<?php

class VES_VendorsBilling_Model_Source_Vendor
{
    public function getAllOptions(){
        $vendors = Mage::getModel('vendors/vendor')->getCollection()->addOrder('vendor_id','ASC');
        $options = array(''=>Mage::helper('vendorsbilling')->__('-- Select Vendor --'));
        foreach($vendors as $vendor){
            $options[$vendor->getId()]	= $vendor->getVendorId();
        }
        return $options;
    }
}