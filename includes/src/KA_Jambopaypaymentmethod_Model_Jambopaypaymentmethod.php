<?php

class KA_Jambopaypaymentmethod_Model_Jambopaypaymentmethod extends Mage_Payment_Model_Method_Abstract
{

    protected $_code = 'jambopaypaymentmethod';
    protected $_formBlockType = 'jambopaypaymentmethod/form_jambopaypaymentmethod';
    protected $_infoBlockType = 'jambopaypaymentmethod/info_jambopaypaymentmethod';
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('jambopaypaymentmethod/payment/redirect', array('_secure' => false));
    }
}