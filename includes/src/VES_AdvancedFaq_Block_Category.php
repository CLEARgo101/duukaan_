<?php
class VES_AdvancedFaq_Block_Category extends VES_AdvancedFaq_Block_Abstract
{
	public function __construct(){
		$this->setTemplate("ves_advancedfaq/category/theme1/category.phtml");
		return parent::__construct();
	}
	
	public function _prepareLayout()
	{
		parent::_prepareLayout();

		$pager = $this->getLayout()->createBlock('advancedfaq/page_html_pager', 'kbase.pager')
			->setCollection($this->getFaqs());
		//$pager->setAvailableLimit($limit);
		$this->setChild('pager', $pager);
		
		if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {

			$link = array(
					'label'	=> $this->getHomeTitle(),
					'title'	=> $this->getHomeTitle(),
					'link'	=> Mage::helper('advancedfaq')->getUrlBase().$this->getUrlKey(),
					//'link'	=> Mage::helper('advancedfaq')->getUrlBase()."faq/home",
			);
			
			if(Mage::registry("current_vendor")){
				$vendor = Mage::registry("current_vendor");
				$url = Mage::helper("vendorspage")->getUrl($vendor);
				$link['link'] = Mage::helper("vendorspage")->getUrl($vendor,$this->getUrlKey());
				//$url= $this->getUrl($vendor->getData("vendor_id")."/".$this->getUrlKey()."/new");
			}
			else{
				$url = Mage::getUrl();
			}
			

			
            $breadcrumbsBlock->addCrumb('home', array(
                'label'	=> Mage::helper('advancedfaq')->__('Home'),
                'title'	=> Mage::helper('advancedfaq')->__('Home'),
                'link'	=> $url,
            ))->addCrumb('kbasehome', $link)
            ->addCrumb('category_'.$this->getCategory()->getId(), array(
                'label'	=> $this->getCategory()->getTitle(),
                'title'	=> $this->getCategory()->getTitle(),
            ));
        }
		if ($headBlock = $this->getLayout()->getBlock('head')) {
			
			$title = $this->getCategory()->getMetaTitle();
			if(!$title) $title = $this->getCategory()->getTitle().' - '.$this->getHomeTitle();
			$headBlock->setTitle($title);
			
			if($keywords = Mage::helper("advancedfaq")->getMetaKeyword()){
				$headBlock->setKeywords($keywords);
			}
			if($metaDescription = Mage::helper("advancedfaq")->getMetaDescription()){
				$headBlock->setDescription($metaDescription);
			}
			
		}
		return $this;
	}
	
	public function getPagerHtml(){
		return $this->getChildHtml('pager');
	}
	
	/**
	 * Get current category
	 * @return VES_AdvancedFaq_Model_Category
	 */
    public function getCategory(){ 
        return Mage::registry('kbase_category');
        
    }
    
    /**
     * Get all faqs related to current category
     * @return VES_AdvancedFaq_Model_Mysql4_Faq_Collection
     */
    public function getFaqs(){
	   	if (!$this->hasData('faqs')) {
        	$category = $this->getCategory();
        	$storeId = Mage::app()->getStore()->getId();
	    	$storeCond = array(
	    		array('finset'=>$storeId),
	    		array('finset'=>0),
	    	);
            $faqs = Mage::getModel("advancedfaq/faq")->getCollection()
            	->addFieldToFilter('category_id',array('eq'=>$category->getData('category_id')))
            	->addFieldToFilter('show_on',array("eq"=>VES_AdvancedFaq_Model_Status::STATUS_ENABLED))
            	->setOrder('created_time','DESC');;
            
	   	if(Mage::registry("current_vendor")){
    		$vendor =  Mage::registry("current_vendor");
    		$faqs->addFieldToFilter('vendor_id',$vendor->getId());
    	}
            	
            $this->setData('faqs',$faqs);
        }
        return $this->getData('faqs');
	}
	/**
	 * 
	 * get Description Chart
	 */
	public function getDescriptionChart(){
		return Mage::helper('advancedfaq')->getDescriptionLength() ;
	}
}