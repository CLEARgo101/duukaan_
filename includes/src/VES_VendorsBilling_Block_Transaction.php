<?php
class VES_VendorsBilling_Block_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'transaction';
    $this->_blockGroup = 'vendorsbilling';
    $this->_headerText = Mage::helper('vendorsbilling')->__('Billing Transactions');
    $this->_addButtonLabel = Mage::helper('vendorsbilling')->__('New Transaction');
    parent::__construct();
    $this->_removeButton('add');
  }
}