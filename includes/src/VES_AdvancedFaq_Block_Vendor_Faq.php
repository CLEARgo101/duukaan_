<?php
class VES_AdvancedFaq_Block_Vendor_Faq extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'vendor_faq';
    $this->_blockGroup = 'advancedfaq';
    $this->_headerText = Mage::helper('advancedfaq')->__('Faq Manager');
    $this->_addButtonLabel = Mage::helper('advancedfaq')->__('Add Faq');
    parent::__construct();
  }
}
