<?php

class VES_VendorsBilling_Block_Adminhtml_Vendor_Transaction_Grid extends VES_VendorsBilling_Block_Transaction_Grid{
	protected function _prepareLayout(){
		parent::_prepareLayout();
	}
	
	protected function _prepareCollection(){
		$collection = Mage::getModel('vendorsbilling/transaction')->getCollection();
		$vendorTbl 	= $collection->getTable('vendors/vendor');
		$collection->getSelect()->join(array('ves_table'=>$vendorTbl),'main_table.vendor_id = ves_table.entity_id',array('vendor_identifier'=>'vendor_id'));

		$this->setCollection($collection);
		return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
	}
	
	protected function _prepareColumns(){
		parent::_prepareColumns();
		$this->addColumnAfter('vendor_identifier', array(
            'header'    => $this->__('Vendor Id'),
            'index'     => 'vendor_identifier',
			'renderer'	=> new VES_VendorsBilling_Block_Widget_Grid_Column_Renderer_Vendor(),
			'width'		=> '150px',
        ),'created_at');
        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
	}
}