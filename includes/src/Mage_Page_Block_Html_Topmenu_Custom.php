<?php
class Mage_Page_Block_Html_Topmenu_Custom extends Mage_Page_Block_Html_Topmenu {
    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     * @deprecated since 1.8.2.0 use child block catalog.topnav.renderer instead
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        if(isset($_GET['debug'])) {
            //var_dump($children);
        }
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            if ($child->hasChildren()) {
                $html .= '<div class="mb-show-child"></div>';
            }
            //$value = Mage::getResourceModel('catalog/category')->getAttributeRawValue($child->getId(), "catalog_main_menu_icon", Mage::app()->getStore()->getId());

//            var_dump($child->getData('icon'));
            $icon = $child->getData('icon');
            $_icon_html = '';
            if(isset($icon)) {
                $_icon_html .= '<img src="'.Mage::getBaseUrl('media').'catalog/category/'.$icon.'" class="category-icon" />';
            }

            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '>'.$_icon_html.'<span>'
                . $this->escapeHtml($child->getName()) . '</span></a>';
            if ($child->hasChildren()) {
                //if (!empty($childrenWrapClass)) {
                    $html .= '<div class="categories-menu-child-wrapper">';
                //}
                //$html .= '<ul class="level' . $childLevel . '">';
                $html .= $this->_getChildMenuHtml($child, $childrenWrapClass);
                //$html .= '</ul>';
                //if (!empty($childrenWrapClass)) {

                $html .= '</div>';
                //}
            }
            /*if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . '">';
                $html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            } */
            $html .= '</li>';

            $counter++;
        }
        return $html;
    }
    protected function _getChildMenuHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';
        $html .= '<ul class="categories-menu-child-item-col ">';
        $html .= '</ul>';

        $column_data = array();
        $parent_id_count = 1;
        foreach ($children as $child) {

            $no_child_class = '';
            if(!$child->hasChildren()) {
                $no_child_class = 'no-child';
            }
            //$html .= '<ul class="categories-menu-child-item-col '.$no_child_class.'">';
            //$child->setLevel($childLevel);
            //$child->setIsFirst($counter == 1);
            //$child->setIsLast($counter == $childrenCount);
            //$child->setPositionClass($itemPositionClassPrefix . $counter);

            /*$outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }*/
            $item = array();
            $item['html'] = '<li class="categories-menu-child-item title '.$no_child_class.'">';
            $item['html'] .= '<a href="' . $child->getUrl() . '"><span>'. $this->escapeHtml($child->getName()) . '</span></a>';
            $item['html'] .= '</li>';
            $item['is_parent'] = true;
            $child_count = 0;
            if ($child->hasChildren()) {
                $_column_children = $child->getChildren();
                foreach ($_column_children as $_item) {
                    $child_count++;
                }
            }
            $item['number_of_child'] = $child_count;
            $item['id'] = $parent_id_count;
            $column_data[] = $item;

            if ($child->hasChildren()) {
                foreach ($_column_children as $_item) {
                    $item = array();
                    $item['html'] = '<li>';
                    $item['html'] .= '<a href="' . $_item->getUrl() . '" ><span>'
                        . $this->escapeHtml($_item->getName()) . '</span></a>';
                    $item['html'] .= '</li>';
                    $item['parent_id'] = $parent_id_count;
                    $column_data[] = $item;
                }
            }
            $parent_id_count++;
            /*if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . '">';
                $html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            } */

//            $counter++;

        }

        $counter = 1;
        $number_of_item_per_column = 13;
        foreach($column_data as $item) {
            if($counter == 1) {
                $html .= '<ul class="categories-menu-child-item-col">';
            }
            if($counter > 1){
                if ($item['is_parent'] == true) {

                    //$html .= ($item['number_of_child']+1) .' -- '.($number_of_item_per_column - $counter);
                    if(($number_of_item_per_column - $counter) < ($item['number_of_child']+1)) {
                        $html .= '</ul>';
                        $html .= '<ul class="categories-menu-child-item-col">';
                        $html .=  $item['html'];
                        $counter = 2;
                        continue;
                    }
                }
            }
            $html .=  $item['html'];



            if($counter == $number_of_item_per_column) {
                $html .= '</ul>';
                $counter=1;
            } else {
                $counter++;
            }
        }
        if($counter < $number_of_item_per_column) {
            $html .= '</ul>';
        }


        return $html;
    }
}