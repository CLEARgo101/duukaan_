<?php
// app/code/local/Envato/Custompaymentmethod/Block/Form/Custompaymentmethod.php
class KA_Jambopaypaymentmethod_Block_Form_Jambopaypaymentmethod extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('jambopaypaymentmethod/form/jambopaypaymentmethod.phtml');
    }
}