<?php

/************************************************************
 * Class VES_VendorsBilling_Model_Observer
 *
 * only ADVANCED X MODE is completed.other are not completed.
 ***********************************************************/
class VES_VendorsBilling_Model_Observer
{
    /**
     * Add credit for vendor when an invoice is created.
     * @param unknown_type $observer
     */
    public function sales_order_invoice_save_after($observer){
        $invoice	= $observer->getInvoice();
        $order		= $invoice->getOrder();
        /* Add money to vendor account*/
        if($invoice->getState() != Mage_Sales_Model_Order_Invoice::STATE_PAID) return;
        
    	if(!$order->getVendorId()) {
	    	/********************
			 * Vendor Made payment to pay for his billing account.
			 *******************/
			foreach($invoice->getAllItems() as $item) {
				$product = Mage::getModel('catalog/product')->load($item->getProductId());
				if($product->getData('is_billing_vendor') == '1' && $product->getData('related_vendor_id') != '') {
					$amount		= $item->getRowTotal();
					$fee        = 0;
					/*Return if the transaction is exist.*/
	                $trans 		= Mage::getModel('vendorsbilling/transaction')->getCollection()
	                    ->addFieldToFilter('type','billing')
	                    ->addFieldToFilter('additional_info',array('like'=>'%order_item|'.$item->getOrderItem()->getId().'%'))
	                ;
	                if($trans->count()) return;
                
					$data = array(
						'related_vendor_id'	=> $product->getData('related_vendor_id'),
						'type'		=> 'billing',
						'amount'	=> $amount,
						'fee'		=> $fee,
						'order'		=> $order,
						'invoice'	=> $invoice,
						'order_item'	=> $item->getOrderItem(),
					);
					Mage::getModel('vendorsbilling/type')->process($data);
				}
				else {
					return;
				}
			}
		}
        
		
        /*Ignore commission calculation for individual payment method*/
    	$paymentMethod = $order->getPayment()->getMethod();
    	$flag = Mage::getStoreConfig('payment/'.$paymentMethod.'/ignore_commission_calculation');
    	if($flag) return;
    	
        
        switch(Mage::helper('vendors')->getMode()) {
            /*************************************
             * ADVANCED X MODE
             ************************************/
        	case 'advanced':
            case 'advanced_x':
                $vendor		= Mage::getModel('vendors/vendor')->load($order->getVendorId());
                /*Do nothing if the vendor is not exist*/
                if(!$vendor->getId()) return;

                /*Return if the transaction is exist.*/
                $trans 		= Mage::getModel('vendorsbilling/transaction')->getCollection()
                    ->addFieldToFilter('type','order_commission')
                    ->addFieldToFilter('additional_info',array('like'=>'%invoice|'.$invoice->getId().'%'))
                ;
                if($trans->count()) return;

                /**
                 * caculating amount and free
                 * amount = current order fee
                 * fee = 0
                 */
               // $totalAmountForFeeCalculation   = Mage::helper('vendors')->getTotalAmountForFeeCalculation($invoice);
                
              //  $amount		                    = Mage::getModel('vendors/group')->load($vendor->getGroupId())
                                                 //   ->getFeeAmount($totalAmountForFeeCalculation);
                $amount = $this->_caculatorFeeComisstion($invoice,$vendor);
                
				if(!$amount || $amount == 0) return;
                $fee = 0;
                $data = array(
                    'vendor'	=> $vendor,
                    'type'		=> 'order_commission',
                    'amount'	=> $amount,
                    'fee'		=> $fee,
                    'order'		=> $order,
                    'invoice'	=> $invoice,
                );
                Mage::getModel('vendorsbilling/type')->process($data);
            break;

            /*************************************
             * GENERAL MODE
             ************************************/
            case 'general':
                
            break;
        }
    }

    
    private function _caculatorFeeComisstion($invoice,$vendor){
        $storeId       = $invoice->getStoreId();
        $websiteId     = Mage::app()->getStore($storeId)->getWebsiteId();
        $vendorGroupId = $vendor->getGroupId();
        $fee = 0;//fix undefined var
        foreach($invoice->getAllItems() as $invoiceItem){
            
            $orderItem 	= $invoiceItem->getOrderItem();
            if($orderItem->getParentItemId()) continue;
            
            $product    = Mage::getModel('catalog/product')->load($orderItem->getProductId());
            
            $ruleCollection = Mage::getModel('vendorscommission/rule')->getCollection()
            ->addFieldToFilter('vendor_group_ids',array('finset'=>$vendorGroupId))
            ->addFieldToFilter('website_ids',array('finset'=>$websiteId))
            ->addFieldToFilter('is_active',VES_VendorsCommissionCalculation_Model_Rule::STATUS_ENABLED)
            ;

            $today = Mage::getModel('core/date')->date();
            $ruleCollection->getSelect()
            ->where('(from_date IS NULL OR from_date<=?) AND (to_date IS NULL OR to_date>=?)',$today,$today)
            ->order('priority ASC');
    
            $commission    = 0;
             
            if($ruleCollection->count()){
                $ruleDescriptionArr = array();
                 
                foreach($ruleCollection as $rule){
                    /*If the product is not match with the conditions just continue*/
                    if(!$rule->getConditions()->validate($product)) continue;
                   $tmpFee = 0;
                    switch($rule->getData('commission_by')){
                        case VES_VendorsCommissionCalculation_Model_Rule::COMMISSION_BY_FIXED_AMOUNT:
                            $tmpFee = $rule->getData('commission_amount');
                            break;
                        case VES_VendorsCommissionCalculation_Model_Rule::COMMISSION_BY_PERCENT_PRODUCT_PRICE:
    						if(!$invoiceItem->getData('base_row_total')){
    							$baseRowTotal = ($invoiceItem->getData('price_incl_tax') * $invoiceItem->getData('qty')) - $invoiceItem->getData('base_tax_amount');
    							$invoiceItem->setData('base_row_total',$baseRowTotal);
    						}
                            switch($rule->getData('commission_action')){
                                case VES_VendorsCommissionCalculation_Model_Rule::COMMISSION_BASED_PRICE_INCL_TAX:
                                    $amount = $invoiceItem->getData('base_row_total') + $invoiceItem->getData('base_tax_amount');
                                    break;
                                case VES_VendorsCommissionCalculation_Model_Rule::COMMISSION_BASED_PRICE_EXCL_TAX:
                                    $amount = $invoiceItem->getData('base_row_total');
                                    break;
                                case VES_VendorsCommissionCalculation_Model_Rule::COMMISSION_BASED_PRICE_AFTER_DISCOUNT_INCL_TAX:
                                    $amount = $invoiceItem->getData('base_row_total') - $invoiceItem->getData('base_discount_amount') + $invoiceItem->getData('base_tax_amount');
                                    break;
                                case VES_VendorsCommissionCalculation_Model_Rule::COMMISSION_BASED_PRICE_AFTER_DISCOUNT_EXCL_TAX:
                                    $amount = $invoiceItem->getData('base_row_total')  - $invoiceItem->getData('base_discount_amount');
                                    break;
                                default:
                                    $amount = $invoiceItem->getData('base_row_total')  - $invoiceItem->getData('base_discount_amount');
                            }
                            $tmpFee = ($rule->getData('commission_amount') * $amount)/100;
                            break;
                    }
                    $tmpFeeWithCurrency = Mage::app()->getLocale()->currency($invoice->getOrder()->getBaseCurrencyCode())->toCurrency($tmpFee,array('precision'=>2));
    
                    $ruleDescriptionArr[] = $rule->getDescription().": -".$tmpFeeWithCurrency;
                    
                    $fee +=  $tmpFee;
                    
                    /*Break if the flag stop rules processing is set to 1*/
                    if($rule->getData('stop_rules_processing')) break;
                }
            }
        }
         
        return $fee;
    }
    
    
    /**
     * Add last 5 credit transactions grid to dashboard
     * @param Varien_Event_Observer $observer
     */
    public function vendor_dashboard_grids_preparelayout(Varien_Event_Observer $observer){
        $grids = $observer->getGrids();
        $grids->addTab('last_5_credit_transaction', array(
            'label'     => $grids->__('Last 5 Transactions'),
            'content'   => $grids->getLayout()->createBlock('vendorsbilling/dashboard_transaction_grid')->toHtml(),
            'active'    => true
        ));
    }


    public function ves_vendors_account_edit_tab_main_before(Varien_Event_Observer $observer){
        if(!Mage::registry('vendors_data') || !Mage::registry('vendors_data')->getId()) return;

        $form = $observer->getForm();
        $fieldset = $form->addFieldset('billing_form', array('legend'=>Mage::helper('vendors')->__('Billing Information'),'class'=>'fieldset-wide'));
        $fieldset->addType('billing_info','VES_VendorsBilling_Block_Form_Element_Billing_Info');
        $fieldset->addField('billing', 'billing_info', array(
            'label'     => Mage::helper('vendors')->__('Billing'),
            'name'      => 'billing',
        ));
    }
}