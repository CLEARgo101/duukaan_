<?php

class VES_VendorsBilling_Model_Resource_Transaction extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
    {    
        $this->_init('vendorsbilling/transaction', 'transaction_id');
    }
}