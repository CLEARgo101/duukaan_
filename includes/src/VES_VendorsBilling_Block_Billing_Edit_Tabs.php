<?php

class VES_VendorsBilling_Block_Billing_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('vendors_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('vendorsbilling')->__('Billing Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('main_section', array(
          'label'     => Mage::helper('vendorsbilling')->__('Main'),
          'title'     => Mage::helper('vendorsbilling')->__('Main'),
          'content'   => $this->getLayout()->createBlock('vendorsbilling/billing_edit_tab_main')->toHtml(),
      ));
      return parent::_beforeToHtml();
  }
}