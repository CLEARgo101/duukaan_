<?php
class VES_AdvancedFaq_Block_Faq extends VES_AdvancedFaq_Block_Abstract
{
	public function _prepareLayout()
    {
    	if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
    		$category = $this->getCategory();
    		$link = array(
                'label'	=> $this->getHomeTitle(),
                'title'	=> $this->getHomeTitle(),
                'link'	=> Mage::helper('advancedfaq')->getUrlBase().$this->getUrlKey(),
            	//'link'	=> Mage::helper('advancedfaq')->getUrlBase()."faq/home",
            );
    		
    		
    		if(Mage::registry("current_vendor")){
    			$vendor = Mage::registry("current_vendor");
    			$url = Mage::helper("vendorspage")->getUrl($vendor);
    			$link['link'] = Mage::helper("vendorspage")->getUrl($vendor,$this->getUrlKey());
    		}
    		else{
    			$url = Mage::getUrl();
    		}
    		
            $breadcrumbsBlock->addCrumb('home', array(
                'label'	=> Mage::helper('advancedfaq')->__('Home'),
                'title'	=> Mage::helper('advancedfaq')->__('Home'),
                'link'	=> $url,
            ))
            ->addCrumb('kbasehome', $link)
            ->addCrumb('new_faq', array(
            		'label'	=> Mage::helper('advancedfaq')->__('New FAQ'),
            		'title'	=> Mage::helper('advancedfaq')->__('New FAQ'),
            ));
        }
    	if ($headBlock = $this->getLayout()->getBlock('head')) {
			
    		if($keywords = Mage::helper("advancedfaq")->getMetaKeyword()){
				$headBlock->setKeywords($keywords);
			}
			if($metaDescription = Mage::helper("advancedfaq")->getMetaDescription()){
				$headBlock->setDescription($metaDescription);
			}
			
		}
		return parent::_prepareLayout();
    }
    
    /**
     * Get current Article
     * @return VES_Kbase_Model_Faq
     */
 	public function getFaq(){ 
        return Mage::registry('kbase_faq');
    }
    /**
     * Get Current Category
     * @return VES_AdvancedFaq_Model_Category
     */
    public function getCategory(){
    	$storeId = Mage::app()->getStore()->getId();
    	$data = array();
    	$con = array(
    		array('finset'=>$storeId),
    		array('finset'=>0),
    	);

    	$category = Mage::getModel("advancedfaq/category")->getCollection()->addFieldToFilter('store_id',$con);
    	

   		 if(Mage::registry("current_vendor")){
    		$vendor =  Mage::registry("current_vendor");
    		$category->addFieldToFilter('vendor_id',$vendor->getId());
    	}
    	
    	return $category;
    	//return Mage::registry('kbase_category');
    }
    /** check enable rating
     * 
     */
    public function isEnableRating(){
    	if(Mage::helper('advancedfaq')->getArticlesRating() == 1) return true;
    	return false;
    }
	/**
	 * get Url rating
	 */
    public function getUrlRating($id){
    	/*
    	if(Mage::helper('advancedfaq')->getSecureUrl() == 1){
    		$url = $this->getUrl("vendorspage/article/vote",array('id'=>$id,'_secure'=>true));
    	}
    	else{
    		$url = $this->getUrl("vendorspage/article/vote",array('id'=>$id));
    	}
    	*/
    	$vendor =  Mage::registry("current_vendor");
    	$url = Mage::helper("vendorspage")->getUrl($vendor,"article/vote/id/".$id);
    	return $url;
    }
    /**
     * get Url New
     */
    public function getUrlNew($id){
    	/*
    	if(Mage::helper('advancedfaq')->getSecureUrl() == 1){
    		$url = $this->getUrl("faq/new/save",array('id'=>$id,'_secure'=>true));
    		if(Mage::registry("current_vendor")){
    			$vendor =  Mage::registry("current_vendor");
    			$url = $this->getUrl("faq/new/save",array('id'=>$id,"vendor_id"=>$vendor->getId(),'_secure'=>true));
    		}
    	}
    	else{

    		$url = $this->getUrl("faq/new/save",array('id'=>$id));
    		if(Mage::registry("current_vendor")){
    			$vendor =  Mage::registry("current_vendor");
    			$url = $this->getUrl("faq/new/save",array('id'=>$id,"vendor_id"=>$vendor->getId()));
    		}
    	}
    	*/
    	$vendor =  Mage::registry("current_vendor");
    	$url = Mage::helper("vendorspage")->getUrl($vendor,$this->getUrlKey()."/new/save");
    	return $url;
    }

}
