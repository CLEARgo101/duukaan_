<?php
class VES_AdvancedFaq_Block_Abstract extends Mage_Core_Block_Template
{
	public function getUrlKey(){
		return Mage::getStoreConfig('advancedfaq/config/url_key');
	}
	
	public function getCatSuffix(){
		return Mage::getStoreConfig('advancedfaq/config/category_suffix');
	}

	
	public function getHomeTitle(){
		if(Mage::registry("current_vendor")){
			return Mage::helper('vendorsconfig')->getVendorConfig('advancedfaq/advancedfaq/title',Mage::registry('current_vendor')->getId());
		}
		else{
			return Mage::getStoreConfig('advancedfaq/config/title');
		}
	}
	
	public function canShowRatingForm($id){
		$cookie = Mage::getSingleton('core/cookie');
		$ratingIds = explode(',',$cookie->get('ves_advancedfaq_rating_ids'));
		return (!in_array($id, $ratingIds));
	}
	public function getUrlRating($id){
		if(Mage::helper('advancedfaq')->getSecureUrl() == 1){
			$url = $this->getUrl("faq/home/vote",array('id'=>$id,'_secure'=>true));
		}
		else{
			$url = $this->getUrl("faq/home/vote",array('id'=>$id));
		}
		return $url;
	}
	public function getUrlRatingAjax(){
		/*
		if(Mage::helper('advancedfaq')->getSecureUrl() == 1){
			$url = $this->getUrl("faq/home/voteajax",array('_secure'=>true));
		}
		else{
			$url = $this->getUrl("faq/home/voteajax");
		}
		*/
		$vendor = Mage::registry("current_vendor");
		$url = Mage::helper("vendorspage")->getUrl($vendor,$this->getUrlKey()."/home/voteajax");
		return $url;
	}
	/** check enable rating
	 *
	 */
	public function isEnableRating(){
		if(Mage::helper('advancedfaq')->getArticlesRating() == 1) return true;
		return false;
	}
	public function getRecapcharHtml(){
		return Mage::helper('advancedfaq/recapcha')->getRecapchaHtml();
	}
	/**
	 * check customer login
	 * 
	 * 
	 * 
	
	public function isCustomerLogin(){
		if(Mage::helper("advancedfaq")->getAllowAccess() == 1){
			if(Mage::getSingleton('customer/session')->getCustomer()->getId()){
				return true;
			}
			else{
				return false;
			}
		}
		return true;
	}
	 */
}