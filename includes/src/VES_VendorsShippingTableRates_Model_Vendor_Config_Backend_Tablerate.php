<?php
/**
 * Backend for serialized array data
 *
 */
class VES_VendorsShippingTableRates_Model_Vendor_Config_Backend_Tablerate extends Mage_Core_Model_Config_Data
{
    /**
     * Process data after load
     */
    protected function _afterLoad()
    {
       $this->setValue('');
    }


    /**
     * Prepare data before save
     */
    public function beforeSave()
    {
        Mage::getResourceModel('vendorstablerates/tablerate')->uploadAndImport($this);
    }
}
