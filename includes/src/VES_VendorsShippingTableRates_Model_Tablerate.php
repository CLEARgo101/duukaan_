<?php

class VES_VendorsShippingTableRates_Model_Tablerate extends Mage_Core_Model_Abstract
{
	public function _construct()
    {
        parent::_construct();
        $this->_init('vendorstablerates/tablerate');
    }
}