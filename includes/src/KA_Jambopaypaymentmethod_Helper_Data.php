<?php


class KA_Jambopaypaymentmethod_Helper_Data extends Mage_Core_Helper_Abstract
{

//	private $GATEWAY_URL = 'https://www.jambopay.com/JPExpress.aspx';
//	private $BUSINESS_EMAIL = 'online@polestars.org';
//	private $SECREC_KEY = '241C7604-01EC-4051-A4B3-AC02E70272E2';

	function getPaymentGatewayUrl()
	{
		$information = $this->getInformationJambopay();
		if (isset($information['GATEWAY_URL']))
			return $information['GATEWAY_URL'];
		return false;
	}

	function getBusinessEmail()
	{
		$information = $this->getInformationJambopay();
		if (isset($information['BUSINESS_EMAIL']))
			return $information['BUSINESS_EMAIL'];
		return false;
	}

	function getSecrecKey()
	{
		$information = $this->getInformationJambopay();
		if (isset($information['SECREC_KEY']))
			return $information['SECREC_KEY'];
		return false;
	}

	function getResponseUrl()
	{
		return Mage::getUrl('jambopaypaymentmethod/payment/response', array('_secure' => false));
	}

	function getCancelUrl()
	{
		return Mage::getUrl('jambopaypaymentmethod/payment/cancel', array('_secure' => false));
	}

	function getFailUrl()
	{
		return Mage::getUrl('jambopaypaymentmethod/payment/fail', array('_secure' => false));
	}

	public function getInformationJambopay()
	{
		if (KA_CustomBlock_Helper_Data::getSiteEVN() == 'development') {
			$GATEWAY_URL = 'https://www.jambopay.com/JPExpress.aspx';
			$BUSINESS_EMAIL = 'demo@webtribe.co.ke';
			$SECREC_KEY = '6127482F-35BC-42FF-A466-276C577E7DF3';
		} elseif (KA_CustomBlock_Helper_Data::getSiteEVN() == 'stg') {
			$GATEWAY_URL = 'https://www.jambopay.com/JPExpress.aspx';
			$BUSINESS_EMAIL = 'demo@webtribe.co.ke';
			$SECREC_KEY = '6127482F-35BC-42FF-A466-276C577E7DF3';
		} elseif (KA_CustomBlock_Helper_Data::getSiteEVN() == 'live') {
			$GATEWAY_URL = 'https://www.jambopay.com/JPExpress.aspx';
			$BUSINESS_EMAIL = 'support@duukaan.co.ke';
			$SECREC_KEY = '9AB916C1-FE36-4B12-A1C4-AF91F3E18585';
//			$BUSINESS_EMAIL = 'support@duukaan.co.ke';
		}
		return array(
				'GATEWAY_URL' => $GATEWAY_URL,
				'BUSINESS_EMAIL' => $BUSINESS_EMAIL,
				'SECREC_KEY' => $SECREC_KEY,
		);
	}
}