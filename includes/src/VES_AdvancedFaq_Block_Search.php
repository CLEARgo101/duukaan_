<?php
class VES_AdvancedFaq_Block_Search extends VES_AdvancedFaq_Block_Abstract
{

	public function getUrlSearch(){
		$url= $this->getUrl($this->getUrlKey()."/search");
		if(Mage::registry("current_vendor")){
			$vendor = Mage::registry("current_vendor");
			$url = Mage::helper("vendorspage")->getUrl($vendor,$this->getUrlKey()."/search");
			//$url= $this->getUrl($vendor->getData("vendor_id")."/".$this->getUrlKey()."/search");
		}
		return $url;
	}
	
	public function getKeyQuery(){
		$key = "";
		if(Mage::registry("key_query")) $key = Mage::registry("key_query");
		return $key;
	}
	
}