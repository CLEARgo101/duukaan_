<?php
class VES_VendorsQuickAddProduct_Block_Vendor_Widget_Form_Renderer_Fieldset_Category extends Mage_Adminhtml_Block_Template
implements Varien_Data_Form_Element_Renderer_Interface
{
    public function getVendorId() {
        return Mage::getSingleton('vendors/session')->getVendorId();
    }
    protected $_element;

    protected function _construct()
    {
        $this->setTemplate('ves_vendorsqap/widget/form/renderer/fieldset/category.phtml');
    }

    public function getElement()
    {
        return $this->_element;
    }

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->_element = $element;
        return $this->toHtml();
    }

    public function getParentLevel() {
        $rootId= Mage::app()->getStore()->getRootCategoryId();
        $category = Mage::getModel('catalog/category')->load($rootId);
        return $category->getLevel()+1;
    }

    public function getAllParentCategories() {
        $rootId= Mage::app()->getStore()->getRootCategoryId();
        $categories = Mage::getModel('catalog/category')->load($rootId)->getChildrenCategories();
        $categories_parent = array();
        foreach($categories as $category) {
        	$category->load($category->getId());
            $categories_parent[$category->getId()] = array(
                'value'         =>      $category->getId(),
                'attribute_id'  =>      $category->getVesAttributeSet(),
                'label'         =>      $category->getName(),
                'level'         =>      $category->getLevel(),
                'has_children'  =>      $category->hasChildren(),
            );
        }

        return $categories_parent;
    }

   /*  public function getCategoriesJSON() {
        $collection = Mage::getModel('catalog/category')->getCollection()
          ->setStoreId(Mage::app()->getStore()->getStoreId())->load();

        $result = array();
        foreach($collection as $_category) {
            if($_category->hasChildren()) {
                $child = $_category->getChildrenCategories();
                $child_arr = array();
                foreach($child as $_c) {
                    $cat = Mage::getModel('catalog/category')->load($_c->getId());
                    $child_arr[$_c->getId()] = array(
                        'value'         =>          $cat->getId(),
                        'attribute_id'  =>          $cat->getData('ves_attribute_set'),
                        'label'         =>          $cat->getName(),
                        'level'         =>          $cat->getLevel(),
                    );
                }
                $result[$_category->getId()] = $child_arr;
            }
        }

        return json_encode($result);
    } */

    public function getSearchUrl() {
        return $this->getUrl('vendors/qap_ajax/search');
    }
    /**
     * Get load children categories URL
     * @return string
     */
    public function getLoadChildrenUrl(){
        return $this->getUrl('vendors/qap_ajax/loadChildren');
    }
    
    public function getSuccessIconUrl() {
        return $this->getSkinUrl('ves_vendorsqap/images/sprite-feedback-addon.png', array('_default'=>true,'_theme'=>'adminhtml','_package'=>'default'));
    }


    /**
     * Get all categories.
     * @return multitype:multitype:NULL mixed Ambigous <multitype:multitype:NULL, multitype:multitype:NULL multitype:multitype:NULL Ambigous <mixed, NULL, multitype:>  >
     */
    public function getAllCategories() {
        $rootId = Mage::app()->getStore()->getRootCategoryId();
        $parent_categories = Mage::getModel('catalog/category')->load($rootId)->getChildrenCategories();//3 cat

        $result = array();
        foreach($parent_categories as $_parent) {
            $_parent_load = Mage::getModel('catalog/category')->load($_parent->getId());
            $result[] = array(
                'value'             =>      $_parent_load->getId(),
                'attribute_id'      =>      $_parent_load->getVesAttributeSet(),
                'label'             =>      $_parent_load->getName(),
                'level'             =>      $_parent_load->getLevel(),
                'data'              =>      $this->getChildren($_parent_load),
                'has_children'      =>      $_parent_load->hasChildren(),
            );
        }

        return $result;
    }

    /**
     * Get all children categories of a category.
     * @param Mage_Catalog_Model_Category $_parent
     * @return multitype:multitype:NULL mixed multitype:multitype:NULL Ambigous <mixed, NULL, multitype:>
     */
    public function getChildren(Mage_Catalog_Model_Category $_parent) {
        $result = array();
        if($_parent->hasChildren()) {
            /*$children = $_parent->getChildrenCategories()->addAttributeToSelect('ves_attribute_set')->load();*/
			$childrenIds = explode(",",$_parent->getChildren());
			$children = Mage::getModel('catalog/category')->getCollection()->addAttributeToFilter('entity_id',array('in'=>$childrenIds))
			->addAttributeToSelect('ves_attribute_set')->load();

            foreach($children as $_child) {
                $_child_load = Mage::getModel('catalog/category')->load($_child->getId());
                $result[] = array(
                    'value'             =>      $_child_load->getId(),
                    'attribute_id'      =>      $_child_load->getVesAttributeSet(),
                    'label'             =>      $_child_load->getName(),
                    'level'             =>      $_child_load->getLevel(),
                    'data'              =>      $this->getChildren($_child_load),
                    'has_children'      =>      $_child_load->hasChildren(),
                );
            }
        }

        return $result;
    }
    
    public function getLevel1Categories(){
        $rootId = Mage::app()->getStore()->getRootCategoryId();
        $parent_categories = Mage::getModel('catalog/category')->load($rootId)->getChildrenCategories();//3 cat
        
        $result = array();
        foreach($parent_categories as $_parent) {
            $_parent_load = Mage::getModel('catalog/category')->load($_parent->getId());
            $result[] = array(
                'value'             =>      $_parent_load->getId(),
                'attribute_id'      =>      $_parent_load->getVesAttributeSet(),
                'label'             =>      $_parent_load->getName(),
                'level'             =>      $_parent_load->getLevel(),
                'data'              =>      array(),
                'has_children'      =>      $_parent_load->hasChildren(),
            );
        }
        
        return $result;
    }
    
    public function getCategoriesJSON() {
        if($this->getIsUsedAjax(true)){
            return json_encode($this->getLevel1Categories());
        }else{
            $cache = Mage::app()->getCache();
            if(!$cache->load("ves_vendors_add_product_form_categories_json")){
                $cache->save(json_encode($this->getAllCategories()), "ves_vendors_add_product_form_categories_json", array("es_vendors_add_product_form"));
            }
            return $cache->load("ves_vendors_add_product_form_categories_json");
        }
    }
    
    /**
     * Is extension used ajax to load children categories 
     */
    public function getIsUsedAjax($booleanResult = false){
        if($booleanResult) return Mage::getStoreConfig('vendors/product_form/use_ajax');
        return Mage::getStoreConfig('vendors/product_form/use_ajax')?"true":"false";
    }
}