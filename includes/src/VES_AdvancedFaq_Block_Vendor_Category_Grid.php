<?php

class VES_AdvancedFaq_Block_Vendor_Category_Grid extends VES_AdvancedFaq_Block_Adminhtml_Category_Grid
{

	public function __construct()
	{
		parent::__construct();
	}
  protected function _prepareCollection()
  {
      $collection = Mage::getModel('advancedfaq/category')->getCollection();
      $vendorId = Mage::getSingleton('vendors/session')->getVendorId();
      $collection->addFieldToFilter('vendor_id',$vendorId);
      $this->setCollection($collection);
      return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
  }
  protected function _prepareColumns(){
    parent::_prepareColumns();
    $this->removeColumn("status");
     //Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    return $this;
  }
}