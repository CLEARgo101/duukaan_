<?php
class VES_VendorsFeaturedProduct_Block_Vendor_Widget_Feature extends Mage_Core_Block_Template{
//	private $_vendor_id;

	public function _beforeToHtml()
    {
    	if($this->getTemplate()){
    		$this->setTemplate('ves_vendorsfeaturedproduct/'.$this->getTemplate().'.phtml');
    	}else{
    		$this->setTemplate("ves_vendorsfeaturedproduct/list.phtml");
    	}
    	return parent::_beforeToHtml();
    }
    
    public function getFeaturedProducts()
    {
    	if($this->getCategoryId() == null){
    		return Mage::getModel('vendorsfeaturedproduct/featuredproduct')->getFeaturedProducts($this->getVendor()->getId(),false,false,false);
    	}else{
    		return Mage::getModel('vendorsfeaturedproduct/featuredproduct')->getFeaturedProducts($this->getVendor()->getId(),false,$this->getCategoryId(),false);
    	}
    }
	public function getColumnCount(){
		if($this->getColumns()){
			return $this->getColumns();
		}
		
	}
	public function getVendor(){
		return Mage::registry("vendor");
	}
}