<?php

/**
 * Vendor sales orders block
 *
 * @category   	VES
 * @package    	VES_Vendors
 * @author    	Vnecoms Team <support@vnecoms.com>
 */
class VES_VendorsSales_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
	protected function _prepareCollection()
    {
    	if(!Mage::helper('vendors')->isAdvancedMode()) return parent::_prepareCollection();

        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $collection->getSelect()
		        ->joinLeft(array('vendor_table'=>$collection->getTable('vendors/vendor')),'vendor_table.entity_id = main_table.vendor_id',array(
				        'vendor'=>'vendor_id'))
		        ->joinLeft(array('p' => 'sales_flat_order_payment'), 'main_table.entity_id = p.parent_id', array(
				        'p.payment_method' => 'method',
				        'p.base_shipping_amount' => 'base_shipping_amount',
				        'p.shipping_amount' => 'shipping_amount',
		        ))
		        ->joinLeft(array('sp' => 'sales_flat_order'), 'main_table.entity_id = sp.entity_id', array(
				        'sp.shipping_description' => 'shipping_description',
				        'sp.shipping_address_id' => 'shipping_address_id',
		        ))
		        ->joinLeft(array('Shipping'=>'sales_flat_order_address'),'sp.shipping_address_id = Shipping.entity_id',array(
				        'Shipping.city' => 'city',
				        'Shipping.street' => 'street',
				        'Shipping.telephone' => 'telephone',
				        'Shipping.postcode' => 'postcode',
				        'Shipping.region' => 'region',
		        ));
        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    protected function _prepareColumns(){
    	if(!Mage::helper('vendors')->isAdvancedMode()) return parent::_prepareColumns();
     	$this->addColumnAfter('vendor',
            array(
                'header'=> Mage::helper('vendors')->__('Vendor Id'),
                'index' => 'vendor',
            	//'renderer'	=> new VES_VendorsSales_Block_Widget_Grid_Column_Renderer_Text(),
        	),
        'real_order_id');
/*	    $this->addColumnAfter('payment_method', array(
			    'header' => Mage::helper('vendors')->__('Payment Method'),
			    'index'  => 'p.payment_method',
	    ),
			    'shipping_name');*/
	    $this->addColumnAfter('base_shipping_amount', array(
			    'header' => Mage::helper('vendors')->__('Shipping Amount (Base)'),
			    'index'  => 'p.base_shipping_amount',
			    'currency'  => 'order_currency_code',
			    'type'      => 'currency',
	    ),
			    'grand_total');
	    $this->addColumnAfter('shipping_amount', array(
			    'header' => Mage::helper('vendors')->__('Shipping Amount (Purchased)'),
			    'index'  => 'p.shipping_amount',
			    'currency'  => 'order_currency_code',
			    'type'      => 'currency',
	    ),
			    'base_shipping_amount');
	    $this->addColumnAfter('shipping_description', array(
			    'header' => Mage::helper('vendors')->__('Delivery Type'),
			    'index'  => 'sp.shipping_description',
	    ),
			    'status');

	    $this->addColumnAfter('region', array(
			    'header' => Mage::helper('vendors')->__('Shipping Region'),
			    'index'  => 'Shipping.region',
	    ),
			    'shipping_description');

	    $this->addColumnAfter('street', array(
			    'header' => Mage::helper('vendors')->__('Shipping Address'),
			    'index'  => 'Shipping.street',
	    ),
			    'region');

    	return parent::_prepareColumns();
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            $field = ( $column->getFilterIndex() ) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                	if($field == 'status'){
                		$this->getCollection()->addFieldToFilter('main_table.status' , $cond);
                	}elseif($field=='vendor'){
                		$this->getCollection()->addFieldToFilter('vendor_table.vendor_id' , $cond);
                	}elseif($field=='increment_id'){
                		$this->getCollection()->addFieldToFilter('main_table.increment_id' , $cond);
                	}elseif($field=='store_id'){
                		$this->getCollection()->addFieldToFilter('main_table.store_id' , $cond);
                	}elseif($field=='created_at'){
                		$this->getCollection()->addFieldToFilter('main_table.created_at' , $cond);
                	}else{
                		$this->getCollection()->addFieldToFilter($field , $cond);
                	}
                }
            }
        }
        return $this;
    }
}
